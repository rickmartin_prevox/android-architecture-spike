package com.tauongames.hubble.content;

import org.json.JSONException;
import org.json.JSONObject;

import com.tauongames.hubble.utils.JsonUtils;
import com.tauongames.hubble.websocket.WebSocketMediator.EEventType;

public class WebSocketEvent {

	private static final String EVENT_TYPE="event_type";
	private static final String METADATA="metadata";
	private static final String STICKER_ID="sticker_id";
	private static final String YUBL_ID="yubl_id";
	private static final String CONVERSATION_ID="conversation_id";

	private EEventType iEventType=EEventType.EUnknown;
	private String iConversationId;
	private String iYublId;
	private int iStickerId;

	public WebSocketEvent(EEventType aType, final String aConversationId, final String aYublId, int aStickerId) {
		iEventType=aType;
		iConversationId=aConversationId;
		iYublId=aYublId;
		iStickerId=aStickerId;
	}

	public WebSocketEvent(final String aEvent) throws JSONException {
		JSONObject jObj=new JSONObject(aEvent);
		System.out.println("RICK: EVENT="+jObj);
		String t=JsonUtils.getString(jObj, EVENT_TYPE, "");
		if(t.length()>0) iEventType=EEventType.getByName(t);
		if(jObj.has(METADATA)) {
			JSONObject metadata=jObj.getJSONObject(METADATA);
			iConversationId=JsonUtils.getString(metadata, CONVERSATION_ID, "");
			iYublId=JsonUtils.getString(metadata, YUBL_ID, "");
			String sid=JsonUtils.getString(metadata, STICKER_ID, "");
			if(sid.length()>0) iStickerId=Integer.parseInt(sid);
		}
	}

	public EEventType getEventType() {
		return iEventType;
	}

	public String getConversationId() {
		return iConversationId;
	}

	public String getYublId() {
		return iYublId;
	}

	public int getStickerId() {
		return iStickerId;
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append(" TYPE=").append(iEventType);
		sb.append(" CONV=").append(iConversationId);
		sb.append(" YUBL=").append(iYublId);
		sb.append(" STICKER=").append(iStickerId);
		return sb.toString();
	}

	public boolean equals(WebSocketEvent aEvent) {
		boolean rc=
				(iEventType==aEvent.iEventType&&iConversationId.equals(aEvent.iConversationId)
						&&iYublId.equals(aEvent.iYublId)&&iStickerId==aEvent.iStickerId);
		System.out.println("RICK: CHECKING EQUALITY "+rc+" "+aEvent+" "+this);
		return rc;
	}
}
