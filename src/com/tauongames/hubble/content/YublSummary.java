package com.tauongames.hubble.content;

import org.json.JSONException;
import org.json.JSONObject;

import com.tauongames.hubble.utils.JsonUtils;

public class YublSummary {

	private static final String ID="id";
	private static final String CREATED_AT="created_at";
	private static final String READ="read";
	private static final String SUMMARY="summary";
	private static final String BACKGROUND_COLOUR="background_color";
	private static final String HEIGHT="height";
	private static final String ELEMENTS="elements";
	private static final String CREATOR="creator";
	private static final String FIRST_NAME="first_name";
	private static final String DISPLAY_NAME="display_name";
	private static final String LAST_ONLINE="last_online";
	private static final String BIOGRAPHY="biography";
	private static final String PROFILE_IMAGE="profile_image";

	private String iId;
	private String iCreatedAt;
	private boolean iRead;
	private String iColour;
	private int iHeight;
	private String iCreatorId;
	private String iFirstName;
	private String iDisplayName;
	private String iProfileImage;
	private String iBiography;
	private String iLastOnline;

	public YublSummary(JSONObject aJObj) throws JSONException {
		iId=JsonUtils.getString(aJObj, ID, "");
		iCreatedAt=JsonUtils.getString(aJObj, CREATED_AT, "");
		iRead=JsonUtils.getBoolean(aJObj, READ, false);
		if(aJObj.has(SUMMARY)) {
			JSONObject summary=aJObj.getJSONObject(SUMMARY);
			iColour=JsonUtils.getString(summary, BACKGROUND_COLOUR, "");
			iHeight=JsonUtils.getInteger(summary, HEIGHT, 0);
		}
		if(aJObj.has(ELEMENTS)) {}
		if(aJObj.has(CREATOR)) {
			JSONObject creator=aJObj.getJSONObject(CREATOR);
			iCreatorId=JsonUtils.getString(creator, ID, "");
			iFirstName=JsonUtils.getString(creator, FIRST_NAME, "");
			iDisplayName=JsonUtils.getString(creator, DISPLAY_NAME, "");
			iLastOnline=JsonUtils.getString(creator, LAST_ONLINE, "");
			iBiography=JsonUtils.getString(creator, BIOGRAPHY, "");
			iProfileImage=JsonUtils.getString(creator, PROFILE_IMAGE, "");
		}
	}

	public String getId() {
		return iId;
	}

	public String getCreatedAt() {
		return iCreatedAt;
	}

	public boolean read() {
		return iRead;
	}

	public String getColour() {
		return iColour;
	}

	public int getHeight() {
		return iHeight;
	}

	public String getProfileImage() {
		return iProfileImage;
	}

	public String getLastOnline() {
		return iLastOnline;
	}

	public String getFirstName() {
		return iFirstName;
	}

	public String getDisplayName() {
		return iDisplayName;
	}

	public String getBiography() {
		return iBiography;
	}

	public String getCreatorId() {
		return iCreatorId;
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("ID=").append(iId);
		sb.append(" CREATED=").append(iCreatedAt);
		sb.append(" READ=").append(iRead);
		sb.append(" COLOUR=").append(iColour);
		sb.append(" HEIGHT=").append(iHeight);
		sb.append(" IMAGE=").append(iProfileImage);
		sb.append(" FIRST=").append(iFirstName);
		sb.append(" DISPLAY=").append(iDisplayName);
		sb.append(" BIO=").append(iBiography);
		sb.append(" CREATOR=").append(iCreatorId);
		return sb.toString();
	}
}
