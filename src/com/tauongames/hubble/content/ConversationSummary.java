package com.tauongames.hubble.content;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;

import com.tauongames.hubble.utils.JsonUtils;

public class ConversationSummary {

	private static final String ID="id";
	private static final String UPDATED_AT="updated_at";
	private static final String HAS_CUSTOM_ICON="has_custom_icon";
	private static final String USERS="users";
	private static final String YUBLS="yubls";

	private String iId;
	private String iUpdatedAt;
	private boolean iHasCustomIcon;
	private HashMap<String, UserSummary> iUsers=new HashMap<String, UserSummary>();
	private HashMap<String, YublSummary> iYubls=new HashMap<String, YublSummary>();

	public ConversationSummary(JSONObject aJObj) throws JSONException {
		iId=JsonUtils.getString(aJObj, ID, "");
		iUpdatedAt=JsonUtils.getString(aJObj, UPDATED_AT, "");
		iHasCustomIcon=JsonUtils.getBoolean(aJObj, HAS_CUSTOM_ICON, false);
		if(aJObj.has(USERS)) {
			JSONArray jArr=aJObj.getJSONArray(USERS);
			for(int a=0;a<jArr.length();a++) {
				UserSummary us=new UserSummary(jArr.getJSONObject(a));
				iUsers.put(us.getId(), us);
			}
		}
		if(aJObj.has(YUBLS)) {
			JSONArray jArr=aJObj.getJSONArray(YUBLS);
			for(int a=0;a<jArr.length();a++) {
				YublSummary ys=new YublSummary(jArr.getJSONObject(a));
				iYubls.put(ys.getId(), ys);
			}
		}
	}

	public String getId() {
		return iId;
	}

	public String getUpdatedAt() {
		return iUpdatedAt;
	}

	public boolean hasCustomIcon() {
		return iHasCustomIcon;
	}

	public int getUserCount() {
		return iUsers.size();
	}

	public Set<String> getUserIds() {
		return iUsers.keySet();
	}

	public UserSummary getUser(String aId) {
		return iUsers.get(aId);
	}

	public int getYublCount() {
		return iYubls.size();
	}

	public Set<String> getYublIds() {
		return iYubls.keySet();
	}

	public YublSummary getYubl(String aId) {
		return iYubls.get(aId);
	}

	public String getProfileImage() {
		YublSummary ys=getLatest();
		return ys.getProfileImage();
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("ID=").append(iId);
		sb.append(" UPDATED=").append(iUpdatedAt);
		sb.append(" CUSTOM=").append(iHasCustomIcon);
		Set<String> users=getUserIds();
		for(String s: users) {
			sb.append(" USER=").append(s);
		}
		Set<String> yubls=getYublIds();
		for(String s: users) {
			sb.append(" YUBL=").append(s);
		}
		return sb.toString();
	}

	@SuppressLint("SimpleDateFormat") public YublSummary getLatest() {
		Iterator<YublSummary> iter=iYubls.values().iterator();
		long latestTime=0;
		YublSummary latestYubl=null;
		while(iter.hasNext()) {
			YublSummary ys=iter.next();
			SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			try {
				Date date=format.parse(ys.getCreatedAt());
				if(date.getTime()>latestTime) {
					latestTime=date.getTime();
					latestYubl=ys;
				}
			} catch(ParseException ex) {
				System.out.println("RICK: DATE EXCEPTION: "+ex.toString());
				ex.printStackTrace();
			}
		}
		return latestYubl;
	}
}
