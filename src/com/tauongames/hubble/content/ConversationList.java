package com.tauongames.hubble.content;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ConversationList {

	private LinkedHashMap<String, ConversationSummary> iConversations=new LinkedHashMap<String, ConversationSummary>();

	public ConversationList(final String aConversationListResponse) throws JSONException {
		JSONArray jArr=new JSONArray(aConversationListResponse);
		for(int a=0;a<jArr.length();a++) {
			ConversationSummary cs=new ConversationSummary((JSONObject) jArr.get(a));
			if(cs.getYublCount()>0) iConversations.put(cs.getId(), cs);
		}
	}

	public int getCount() {
		return iConversations.size();
	}

	public ConversationSummary getConversation(String aId) {
		return iConversations.get(aId);
	}

	public ArrayList<String> getConversationIds() {
		return new ArrayList<String>(iConversations.keySet());
	}

	public ArrayList<String> getProfileImageUrls() {
		ArrayList<String> result=new ArrayList<String>();
		Iterator<ConversationSummary> iter=iConversations.values().iterator();
		while(iter.hasNext()) {
			ConversationSummary cs=iter.next();
			result.add(cs.getProfileImage());
		}
		return result;
	}
}
