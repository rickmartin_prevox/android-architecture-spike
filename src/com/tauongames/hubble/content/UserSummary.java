package com.tauongames.hubble.content;

import org.json.JSONException;
import org.json.JSONObject;

import com.tauongames.hubble.utils.JsonUtils;

public class UserSummary {

	private static final String ID="id";
	private static final String DISPLAY_NAME="display_name";
	private static final String FIRST_NAME="first_name";
	private static final String LAST_NAME="last_name";
	private static final String PROFILE_IMAGE="profile_image";
	private static final String LAST_ONLINE="last_online";
	private static final String BIOGRAPHY="biography";

	private String iId;
	private String iProfileImage;
	private String iDisplayName;
	private String iFirstName;
	private String iLastName;
	private String iLastOnline;
	private String iBiography;

	public UserSummary(JSONObject aJObj) throws JSONException {
		iId=JsonUtils.getString(aJObj, ID, "");
		iDisplayName=JsonUtils.getString(aJObj, DISPLAY_NAME, "");
		iFirstName=JsonUtils.getString(aJObj, FIRST_NAME, "");
		iLastName=JsonUtils.getString(aJObj, LAST_NAME, "");
		iLastOnline=JsonUtils.getString(aJObj, LAST_ONLINE, "");
		iBiography=JsonUtils.getString(aJObj, BIOGRAPHY, "");
		iProfileImage=JsonUtils.getString(aJObj, PROFILE_IMAGE, "");
	}

	public String getId() {
		return iId;
	}

	public String getDisplayName() {
		return iDisplayName;
	}

	public String getFirstName() {
		return iFirstName;
	}

	public String getLastName() {
		return iLastName;
	}

	public String getLastOnline() {
		return iLastOnline;
	}

	public String getBiography() {
		return iBiography;
	}

	public String getProfileImage() {
		return iProfileImage;
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("ID=").append(iId);
		sb.append(" DISP=").append(iDisplayName);
		sb.append(" FIRST =").append(iFirstName);
		sb.append(" LAST=").append(iLastName);
		sb.append(" ONLINE=").append(iLastOnline);
		sb.append(" BIO=").append(iBiography);
		sb.append(" IMAGE=").append(iProfileImage);
		return sb.toString();
	}
}
