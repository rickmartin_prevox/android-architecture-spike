package com.tauongames.hubble.content;

import org.json.JSONException;
import org.json.JSONObject;

import com.tauongames.hubble.utils.JsonUtils;

public class Login {

	private static final String ACCESS_TOKEN="access_token";

	private String iAccessToken;

	public Login(String aLoginResponse) throws JSONException {
		JSONObject jObj=new JSONObject(aLoginResponse);
		iAccessToken=JsonUtils.getString(jObj, ACCESS_TOKEN, "<none>");
	}

	public String getAccessToken() {
		return iAccessToken;
	}
}
