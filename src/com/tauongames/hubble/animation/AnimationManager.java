package com.tauongames.hubble.animation;

import java.util.HashMap;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;

public class AnimationManager {

	public enum EAnimationType {
		EWiggle("wiggle"),
		ESpin("spin"),
		EUnknown("unknown");

		private String iName;

		private EAnimationType(final String aName) {
			iName=aName;
		}

		public static EAnimationType getByName(final String aName) {
			for(EAnimationType at: EAnimationType.values())
				if(at.iName.equals(aName)) return at;
			return EUnknown;
		}
	}

	private static HashMap<EAnimationType, AnimationSet> iAnimations=new HashMap<EAnimationType, AnimationSet>();

	public static void createAnimation(EAnimationType aType) {
		if(iAnimations.containsKey(aType)) return;
		AnimationSet animSet=null;
		Animation anim;
		switch(aType) {
			case EWiggle: {
				int duration=150;
				int delay=duration/2;
				animSet=new AnimationSet(false);
				anim=new RotateAnimation(0, -20, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.75f);
				anim.setDuration(duration/2);
				anim.setFillBefore(true);
				animSet.addAnimation(anim);
				for(int i=4;i>=2;i+=-1) {
					anim=
							new RotateAnimation(-20/(4/i), 20/(4/i), Animation.RELATIVE_TO_SELF, 0.5f,
									Animation.RELATIVE_TO_SELF, 0.75f);
					anim.setDuration(duration);
					anim.setStartOffset(delay);

					animSet.addAnimation(anim);
					delay+=duration;
					anim=
							new RotateAnimation(20/(4/i), -20/(4/(i-1)), Animation.RELATIVE_TO_SELF, 0.5f,
									Animation.RELATIVE_TO_SELF, 0.75f);
					anim.setDuration(duration);
					anim.setStartOffset(delay);

					animSet.addAnimation(anim);
					delay+=duration;
				}
				anim=new RotateAnimation(-5f, 5f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.75f);
				anim.setDuration(duration/2);
				anim.setStartOffset(delay);
				animSet.addAnimation(anim);
				animSet.setFillAfter(true);
				break;
			}
			case ESpin: {
				int duration=150;
				animSet=new AnimationSet(false);
				anim=new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
				anim.setDuration(duration);
				anim.setFillBefore(true);
				anim.setRepeatCount(2);
				animSet.addAnimation(anim);
				break;
			}
			default:
				break;
		}
		if(animSet!=null) iAnimations.put(aType, animSet);
	}

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH) public static void animate(EAnimationType aType, final View aView) {
		if(aType==null||aView==null) return;
		createAnimation(aType);
		AnimationSet animSet=iAnimations.get(aType);
		animSet.cancel();
		aView.clearAnimation();
		if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.ICE_CREAM_SANDWICH) aView.animate().cancel();
		animSet.setAnimationListener(new AnimationListener() {

			@Override public void onAnimationStart(Animation aAnimation) {}

			@Override public void onAnimationEnd(Animation aAnimation) {
				aView.clearAnimation();
			}

			@Override public void onAnimationRepeat(Animation aAnimation) {}

		});
		aView.startAnimation(animSet);
	}
}
