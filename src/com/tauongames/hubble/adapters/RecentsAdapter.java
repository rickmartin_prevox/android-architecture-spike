package com.tauongames.hubble.adapters;

import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tauongames.hubble.Globals;
import com.tauongames.hubble.R;
import com.tauongames.hubble.content.YublSummary;
import com.tauongames.hubble.http.ImageLoader;
import com.tauongames.hubble.views.RoundedImageView;

public class RecentsAdapter extends BaseAdapter {

	private static class ViewHolder {

		public TextView iText;
		public ImageView iImage;
	}

	private ArrayList<String> iConversationIds=new ArrayList<String>();
	private Activity iContext;
	private ImageLoader iImageLoader;

	public RecentsAdapter(Activity aContext) {
		iContext=aContext;
		iImageLoader=new ImageLoader(aContext.getApplicationContext());
	}

	public void setItems(final ArrayList<String> aItems) {
		iConversationIds=aItems;
	}

	@Override public int getCount() {
		return iConversationIds.size();
	}

	@Override public Object getItem(int aPos) {
		return iConversationIds.get(aPos);
	}

	@Override public long getItemId(int aPos) {
		return 0;
	}

	@Override public View getView(int aPos, View aView, ViewGroup aParent) {
		YublSummary ys=Globals.iConversationList.getConversation(iConversationIds.get(aPos)).getLatest();
		View view=aView;
		if(view==null) {
			LayoutInflater inflater=iContext.getLayoutInflater();
			view=inflater.inflate(R.layout.recents_row, null);
			ViewHolder viewHolder=new ViewHolder();
			viewHolder.iText=(TextView) view.findViewById(R.id.title);
			viewHolder.iImage=(RoundedImageView) view.findViewById(R.id.avatar);
			view.setTag(viewHolder);
		}
		ViewHolder holder=(ViewHolder) view.getTag();
		holder.iText.setText(ys.getDisplayName());
		iImageLoader.displayImage(ys.getProfileImage(), holder.iImage);
		return view;
	}
}
