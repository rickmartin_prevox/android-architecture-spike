package com.tauongames.hubble.yubl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tauongames.hubble.utils.JsonUtils;

/**
 * Yubl definition is the in-memory representation of the JSON file that
 * describes the Yubl
 * 
 * @author Rick Martin
 * 
 */
public class YublDefinition {

	private static final String ELEMENTS="elements";
	private static final String BOTTOM_BOUND="bottomBound";
	private static final String TOP_BOUND="topBound";
	private static final String BACKGROUND="background";

	private ArrayList<YublElement> iElements=new ArrayList<YublElement>();
	private int iTopBound;
	private int iBottomBound;
	private YublBackground iBackground;
	private String iId;

	public YublDefinition(final String aFileName) throws JSONException, IOException {
		if(aFileName==null||aFileName.length()==0)
			throw new IllegalArgumentException("YublDefinition: must have valid file name");
		int index=aFileName.lastIndexOf("/");
		iId=aFileName.substring(0, index);
		index=iId.lastIndexOf("/");
		iId=iId.substring(index+1);
		File f=new File(aFileName);
		if(!f.exists())
			throw new IllegalArgumentException("YublDefinition: FILE SPECIFIED DOES NOT EXIST '"+aFileName+"'");
		StringBuilder sb=new StringBuilder();
		BufferedReader br=null;
		br=new BufferedReader(new FileReader(f));
		String line;
		while((line=br.readLine())!=null)
			sb.append(line);
		br.close();
		JSONObject jObj=new JSONObject(sb.toString());
		iTopBound=JsonUtils.getInteger(jObj, TOP_BOUND, 0);
		iBottomBound=JsonUtils.getInteger(jObj, BOTTOM_BOUND, 0);
		if(jObj.has(ELEMENTS)) {
			JSONArray jArr=jObj.getJSONArray(ELEMENTS);
			for(int a=0;a<jArr.length();a++) {
				YublElement ye=new YublElement(jArr.getJSONObject(a));
				iElements.add(ye);
			}
		}
		if(jObj.has(BACKGROUND)) {
			iBackground=new YublBackground(jObj.getJSONObject(BACKGROUND));
		}
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("TOP=").append(iTopBound);
		sb.append(" BOTTOM=").append(iBottomBound);
		for(YublElement ye: iElements)
			sb.append(" ELEMENT: ").append(ye);
		sb.append(" BACKGROUND: ").append(iBackground);
		return sb.toString();
	}

	public int getTopBound() {
		return iTopBound;
	}

	public int getBottomBound() {
		return iBottomBound;
	}

	public YublBackground getBackground() {
		return iBackground;
	}

	public int getElementCount() {
		return iElements.size();
	}

	public String getId() {
		return iId;
	}

	public YublElement getElement(int aIndex) {
		if(aIndex<0||aIndex>iElements.size()-1) return null;
		return iElements.get(aIndex);
	}
}
