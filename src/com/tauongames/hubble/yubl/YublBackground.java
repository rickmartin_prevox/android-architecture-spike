package com.tauongames.hubble.yubl;

import org.json.JSONException;
import org.json.JSONObject;

import com.tauongames.hubble.utils.JsonUtils;

public class YublBackground {

	public static final String TYPE="type";
	public static final String PROPERTY="property";

	public enum EType {
		EColour("color"),
		EUnknown("unknown");

		private String iName;

		private EType(final String aName) {
			iName=aName;
		}

		public static EType getByName(final String aName) {
			for(EType t: EType.values())
				if(t.iName.equals(aName)) return t;
			return EUnknown;
		}
	}

	private EType iType=EType.EUnknown;
	private String iProperty;

	public YublBackground(JSONObject aJObj) throws JSONException {
		iProperty=JsonUtils.getString(aJObj, PROPERTY, "");
		String t=JsonUtils.getString(aJObj, TYPE, "");
		if(t.length()>0) iType=EType.getByName(t);
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("TYPE="+iType);
		sb.append(" PROPERTY=").append(iProperty);
		return sb.toString();
	}

	public EType getType() {
		return iType;
	}

	public String getProperty() {
		return iProperty;
	}
}
