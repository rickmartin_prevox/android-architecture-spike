package com.tauongames.hubble.yubl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.json.JSONException;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.PictureDrawable;
import android.os.Handler;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.caverock.androidsvg.SVG;
import com.tauongames.hubble.Constants;
import com.tauongames.hubble.Globals;
import com.tauongames.hubble.R;
import com.tauongames.hubble.content.YublSummary;
import com.tauongames.hubble.views.YublView;
import com.tauongames.hubble.yubl.ViewUpdater.EUpdateType;
import com.tauongames.hubble.yubl.ViewUpdater.ViewUpdaterData;

public class YublBuilder {

	private static Handler iHandler=new Handler();

	private YublBuilder() {}

	public static YublView getYublPlaceholder(Context aContext, YublSummary aSummary) {
		YublView yubl=new YublView(aContext);
		int height=(int) (((float) aSummary.getHeight()/100.0f)*Globals.iScreenWidth);
		yubl.iWidth=Globals.iScreenWidth;
		yubl.iHeight=height;
		LayoutParams lp=new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, height);
		yubl.setLayoutParams(lp);
		yubl.setBackgroundColor(Color.parseColor(aSummary.getColour()));
		return yubl;
	}

	public static YublData getRealYubl(Context aContext, final String aId, final YublView aView,
			OnClickListener aListener) {
		YublDefinition def=null;
		try {
			def=
					new YublDefinition(android.os.Environment.getExternalStorageDirectory()+"/"+Constants.CACHE_DIR+"/"
							+aId+"/yubl.json");
		} catch(Exception ex) {
			System.out.println("RICK: EXCEPTION CREATING YUBL DEFINITION: "+ex.toString());
			ex.printStackTrace();
			return null;
		}
		YublData data=new YublData(aId, aView);
		boolean rc=buildYubl(aContext, data, def, aListener);
		return rc?data:null;
	}

	private static boolean buildYubl(Context aContext, YublData aData, YublDefinition aDefinition,
			OnClickListener aListener) {
		if(!buildBackground(aDefinition.getBackground(), aData.getView())) return false;
		int elementCount=aDefinition.getElementCount();
		for(int a=0;a<elementCount;a++)
			if(!buildElement(aContext, aData, aDefinition, a, aListener)) return false;
		return true;
	}

	private static boolean buildElement(Context aContext, YublData aData, YublDefinition aDefinition, int aIndex,
			OnClickListener aListener) {
		switch(aDefinition.getElement(aIndex).getType()) {
			case EText:
				return buildTextElement(aContext, aData, aDefinition, aIndex);
			case ESticker:
				return buildStickerElement(aContext, aData, aDefinition, aIndex, aListener);
			default:
				break;
		}
		return false;
	}

	private static boolean buildTextElement(Context aContext, YublData aData, YublDefinition aDefinition, int aIndex) {
		YublElement element=aDefinition.getElement(aIndex);
		TextView tv=new TextView(aContext);
		float dx=element.getX()/100.f;
		float dy=element.getY()/100.f;
		float width=(int) (element.getWidth()*element.getScaleX());
		float height=(int) (element.getHeight()*element.getScaleY());
		FrameLayout.LayoutParams lp=new FrameLayout.LayoutParams((int) width, (int) height);
		lp.setMargins((int) (dx*aData.getView().iWidth-width/2), (int) (dy*aData.getView().iHeight-height/2), 0, 0);
		String text=element.getLabel();
		if(text==null||text.length()==0) text="Label was empty!";
		tv.setText(text);
		tv.setTextSize(30);
		tv.setLayoutParams(lp);
		tv.setRotation(element.getRotation());
		tv.setAlpha(element.getAlpha());
		ElementData ed=new ElementData();
		ed.setView(tv);
		aData.addElementData(ed);
		ViewUpdaterData vud=new ViewUpdaterData();
		vud.iType=EUpdateType.EAddView;
		vud.iYublView=aData.getView();
		vud.iView=tv;
		ViewUpdater vu=new ViewUpdater(vud);
		iHandler.post(vu);
		return true;
	}

	private static boolean buildStickerElement(Context aContext, YublData aData, YublDefinition aDefinition,
			int aIndex, OnClickListener aListener) {
		YublElement element=aDefinition.getElement(aIndex);
		String asset=element.getExtAssetId();
		int left=asset.indexOf(":");
		int right=asset.lastIndexOf(":");
		String baseAssetName=asset.substring(left+1, right);
		String basePath=
				android.os.Environment.getExternalStorageDirectory()+"/"+Constants.EXTERNAL_ASSETS_CACHE_DIR+"/"
						+baseAssetName;
		String jsonPath=basePath+"/"+baseAssetName+".json";
		try {
			YublStandardAssetDefinition assetDef=new YublStandardAssetDefinition(jsonPath);
			int defaultCount=assetDef.getDefaultEventCount();
			for(int a=0;a<defaultCount;a++) {
				String[] tokens=assetDef.getDefaultEvent(a).split(":");
				if(tokens[0].equals("draw")) {
					if(!buildStickerImage(aIndex, aContext, aData, element, assetDef, basePath, tokens[1], aListener))
						return false;
				}
			}
		} catch(JSONException|IOException ex) {
			ex.printStackTrace();
			System.out.println("RICK: EXCEPTION PARSING STICKER JSON "+baseAssetName+" "+ex.toString());
			return false;
		}
		return true;
	}

	private static boolean buildStickerImage(int aIndex, Context aContext, YublData aData, YublElement aElement,
			YublStandardAssetDefinition aAssetDefinition, final String aPath, final String aFileName,
			OnClickListener aListener) {
		float dx=aElement.getX()/100.f;
		float dy=aElement.getY()/100.f;
		float width=(int) (aElement.getWidth()*aElement.getScaleX()*2);
		float height=(int) (aElement.getHeight()*aElement.getScaleY()*2);
		ImageView iv=new ImageView(aContext);
		FrameLayout.LayoutParams lp=new FrameLayout.LayoutParams((int) width, (int) height);
		lp.setMargins((int) (dx*aData.getView().iWidth-width/2), (int) (dy*aData.getView().iHeight-height/2), 0, 0);
		iv.setLayoutParams(lp);
		iv.setRotation(aElement.getRotation());
		iv.setAlpha(aElement.getAlpha());
		iv.setOnClickListener(aListener);
		File file=new File(aPath+"/"+aFileName);
		FileInputStream is=null;
		try {
			is=new FileInputStream(file);
		} catch(Exception ex) {
			System.out.println("RICK: UNABLE TO CREATE INPUT STREAM "+aFileName+" "+ex.toString());
			return false;
		}
		int index=aFileName.lastIndexOf(".");
		String ext=aFileName.substring(index+1);
		if(ext.equalsIgnoreCase("svg")) {
			try {
				iv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
				// TODO: REMEMBER TO REMOVE THIS TEST CODE
				SVG svg=SVG.getFromResource(aContext, R.raw.test);
				Drawable drawable=new PictureDrawable(svg.renderToPicture());
				iv.setImageDrawable(drawable);
			} catch(Exception ex) {
				System.out.println("RICK: UNABLE TO LOAD SVG FILE: "+ex.toString());
			}
		}
		if(ext.equalsIgnoreCase("png")) {
			iv.setImageBitmap(BitmapFactory.decodeStream(is));
		}
		ViewUpdaterData vud=new ViewUpdaterData();
		ElementData ed=new ElementData();
		ed.setView(iv);
		for(int a=0;a<aAssetDefinition.getDownEventCount();a++)
			ed.addDownAction(aAssetDefinition.getDownEvent(a));
		aData.addElementData(ed);
		vud.iType=EUpdateType.EAddView;
		vud.iYublView=aData.getView();
		vud.iView=iv;
		System.out.println("RICK: VUD="+vud.iType+" "+vud.iYublView+" "+vud.iView);
		ViewUpdater vu=new ViewUpdater(vud);
		iHandler.post(vu);
		return true;
	}

	private static boolean buildBackground(YublBackground aDefinition, YublView aView) {
		if(aDefinition==null) return true;
		switch(aDefinition.getType()) {
			case EColour:
				ViewUpdaterData vud=new ViewUpdaterData();
				vud.iType=EUpdateType.EBackgroundColour;
				vud.iYublView=aView;
				vud.iProperty=aDefinition.getProperty();
				ViewUpdater vu=new ViewUpdater(vud);
				iHandler.post(vu);
				break;
			default:
				break;
		}
		return true;
	}
}
