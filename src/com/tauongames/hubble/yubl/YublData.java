package com.tauongames.hubble.yubl;

import java.util.ArrayList;

import android.view.View;

import com.tauongames.hubble.views.YublView;

public class YublData {

	private ArrayList<ElementData> iElementData=new ArrayList<ElementData>();
	private String iId;
	private YublView iView;

	public YublData(final String aId, YublView aView) {
		iId=aId;
		iView=aView;
	}

	public void addElementData(ElementData aData) {
		iElementData.add(aData);
	}

	public String getId() {
		return iId;
	}

	public YublView getView() {
		return iView;
	}

	public ElementData getElementData(View aView) {
		for(ElementData ed: iElementData) {
			if(ed.getView()==aView) return ed;
		}
		return null;
	}

	public ElementData getElementData(int aIndex) {
		if(aIndex<0||aIndex>iElementData.size()-1) return null;
		return iElementData.get(aIndex);
	}

	public int getElementIndex(View aView) {
		for(int a=0;a<iElementData.size();a++)
			if(iElementData.get(a).getView()==aView) return a;
		return -1;
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("ID=").append(iId);
		sb.append(" VIEW=").append(iView);
		for(ElementData ed: iElementData)
			sb.append(" ELEMENT=").append(ed.toString());
		return sb.toString();
	}
}
