package com.tauongames.hubble.yubl;

import java.util.HashMap;

public class ConversationData {

	private HashMap<String, YublData> iYubls=new HashMap<String, YublData>();

	public ConversationData() {}

	public void clear() {}

	public void addYubl(YublData aData) {
		if(aData==null) return;
		String id=aData.getId();
		iYubls.put(id, aData);
	}
}
