package com.tauongames.hubble.yubl;

import org.json.JSONException;
import org.json.JSONObject;

import com.tauongames.hubble.utils.JsonUtils;

public class YublElement {

	public enum EType {
		EText("text"),
		ESticker("sticker"),
		EUnknown("unknown");

		private String iName;

		private EType(final String aName) {
			iName=aName;
		}

		public static EType getByName(final String aName) {
			for(EType t: EType.values())
				if(t.iName.equals(aName)) return t;
			return EUnknown;
		}

	}

	public enum EJustification {
		ECenter("center"),
		ELeft("left"),
		ERight("right"),
		ETop("top"),
		EBottom("bottom"),
		EUnknown("unknown");

		private String iName;

		private EJustification(final String aName) {
			iName=aName;
		}

		public static EJustification getByName(final String aName) {
			for(EJustification j: EJustification.values())
				if(j.iName.equals(aName)) return j;
			return EUnknown;
		}
	}

	private static final String SCALE_X="scaleX";
	private static final String SCALE_Y="scaleX";
	private static final String WIDTH="width";
	private static final String LABEL="label";
	private static final String TYPE="type";
	private static final String HEIGHT="height";
	private static final String JUSTIFICATION="justification";
	private static final String COLOUR="color";
	private static final String ROTATION="rotation";
	private static final String ALPHA="alpha";
	private static final String TEXT_STYLE="textStyle";
	private static final String FONT_FAMILY="fontFamily";
	private static final String X="x";
	private static final String Y="y";
	private static final String EXT_ASSET_ID="extAssetId";

	private float iScaleX;
	private float iScaleY;
	private float iWidth;
	private float iHeight;
	private String iLabel;
	private EType iType=EType.EUnknown;
	private EJustification iJustification=EJustification.EUnknown;
	private String iColour;
	private float iRotation;
	private float iAlpha;
	private int iTextStyle;
	private String iFontFamily;
	private float iX;
	private float iY;
	private String iExtAssetId;

	public float getRotation() {
		return iRotation;
	}

	public float getScaleX() {
		return iScaleX;
	}

	public float getScaleY() {
		return iScaleY;
	}

	public float getX() {
		return iX;
	}

	public float getY() {
		return iY;
	}

	public float getWidth() {
		return iWidth;
	}

	public float getHeight() {
		return iHeight;
	}

	public float getAlpha() {
		return iAlpha;
	}

	public String getLabel() {
		return iLabel;
	}

	public String getColour() {
		return iColour;
	}

	public EType getType() {
		return iType;
	}

	public EJustification getJustification() {
		return iJustification;
	}

	public int getTextStyle() {
		return iTextStyle;
	}

	public String getFontFamily() {
		return iFontFamily;
	}

	public String getExtAssetId() {
		return iExtAssetId;
	}

	public YublElement(JSONObject aJObj) throws JSONException {
		iScaleX=JsonUtils.getFloat(aJObj, SCALE_X, 1);
		iScaleY=JsonUtils.getFloat(aJObj, SCALE_Y, 1);
		iX=JsonUtils.getFloat(aJObj, X, 0.5f);
		iY=JsonUtils.getFloat(aJObj, Y, 0.5f);
		iWidth=JsonUtils.getFloat(aJObj, WIDTH, 100);
		iHeight=JsonUtils.getFloat(aJObj, HEIGHT, 100);
		iRotation=JsonUtils.getFloat(aJObj, ROTATION, 0);
		iAlpha=JsonUtils.getFloat(aJObj, ALPHA, 1);
		iLabel=JsonUtils.getString(aJObj, LABEL, "");
		iColour=JsonUtils.getString(aJObj, COLOUR, "");
		iExtAssetId=JsonUtils.getString(aJObj, EXT_ASSET_ID, "");
		iTextStyle=JsonUtils.getInteger(aJObj, TEXT_STYLE, 0);
		iFontFamily=JsonUtils.getString(aJObj, FONT_FAMILY, "");
		String j=JsonUtils.getString(aJObj, JUSTIFICATION, "");
		if(j.length()>0) iJustification=EJustification.getByName(j);
		String t=JsonUtils.getString(aJObj, TYPE, "");
		if(t.length()>0) iType=EType.getByName(t);
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("TYPE=").append(iType);
		sb.append(" X=").append(iX);
		sb.append(" Y=").append(iY);
		sb.append(" SCALE_X=").append(iScaleX);
		sb.append(" SCALE_Y=").append(iScaleY);
		sb.append(" ALPHA=").append(iAlpha);
		sb.append(" WIDTH=").append(iWidth);
		sb.append(" HEIGHT=").append(iHeight);
		sb.append(" COLOUR=").append(iColour);
		sb.append(" ROTATION=").append(iRotation);
		sb.append(" TEXT STYLE=").append(iTextStyle);
		sb.append(" JUSTIFICATION=").append(iJustification);
		sb.append(" FONT FAMILY=").append(iFontFamily);
		sb.append(" LABEL=").append(iLabel);
		sb.append(" ASSET=").append(iExtAssetId);
		return sb.toString();
	}
}
