package com.tauongames.hubble.yubl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.tauongames.hubble.utils.JsonUtils;

public class YublStandardAssetDefinition {

	public enum EType {
		ESticker("sticker"),
		EUnknown("unknown");

		private String iName;

		private EType(final String aName) {
			iName=aName;
		}

		public static EType getByName(final String aName) {
			for(EType t: EType.values())
				if(t.iName.equals(aName)) return t;
			return EUnknown;
		}
	}

	private static final String TYPE="type";
	private static final String VERSION="version";
	private static final String EVENTS="events";
	private static final String DEFAULT="default";
	private static final String DOWN="down";
	private static final String START="start";
	private static final String REMOTE="remote";

	private EType iType=EType.EUnknown;
	private int iVersion;
	private ArrayList<String> iDefaultEvents=new ArrayList<String>();
	private ArrayList<String> iDownEvents=new ArrayList<String>();
	private ArrayList<String> iStartEvents=new ArrayList<String>();
	private ArrayList<String> iRemoteEvents=new ArrayList<String>();

	public YublStandardAssetDefinition(final String aFileName) throws JSONException, IOException {
		if(aFileName==null||aFileName.length()==0)
			throw new IllegalArgumentException("YublStandardAssetDefinition: must have valid file name");
		File f=new File(aFileName);
		if(!f.exists())
			throw new IllegalArgumentException("YublStandardAssetDefinition: FILE SPECIFIED DOES NOT EXIST '"+aFileName
					+"'");
		StringBuilder sb=new StringBuilder();
		BufferedReader br=null;
		br=new BufferedReader(new FileReader(f));
		String line;
		while((line=br.readLine())!=null)
			sb.append(line);
		br.close();
		JSONObject jObj=new JSONObject(sb.toString());
		String t=JsonUtils.getString(jObj, TYPE, "");
		if(t.length()>0) iType=EType.getByName(t);
		iVersion=JsonUtils.getInteger(jObj, VERSION, 0);
		if(jObj.has(EVENTS)) {
			JSONObject events=jObj.getJSONObject(EVENTS);
			if(events.has(DEFAULT)) {
				JSONArray jArr=events.getJSONArray(DEFAULT);
				int count=jArr.length();
				for(int a=0;a<count;a++) {
					iDefaultEvents.add(jArr.getString(a));
				}
			}
			if(events.has(DOWN)) {
				JSONArray jArr=events.getJSONArray(DOWN);
				int count=jArr.length();
				for(int a=0;a<count;a++) {
					iDownEvents.add(jArr.getString(a));
				}
			}
			if(events.has(START)) {
				JSONArray jArr=events.getJSONArray(START);
				int count=jArr.length();
				for(int a=0;a<count;a++) {
					iStartEvents.add(jArr.getString(a));
				}
			}
			if(events.has(REMOTE)) {
				JSONArray jArr=events.getJSONArray(REMOTE);
				int count=jArr.length();
				for(int a=0;a<count;a++) {
					iRemoteEvents.add(jArr.getString(a));
				}
			}

		}
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("TYPE=").append(iType);
		sb.append(" VERSION=").append(iVersion);
		sb.append(" DEFAULT=").append(iDefaultEvents);
		sb.append(" DOWN=").append(iDownEvents);
		sb.append(" START=").append(iStartEvents);
		sb.append(" REMOTE=").append(iRemoteEvents);
		return sb.toString();
	}

	public EType getType() {
		return iType;
	}

	public int getVersion() {
		return iVersion;
	}

	public int getDefaultEventCount() {
		return iDefaultEvents.size();
	}

	public int getDownEventCount() {
		return iDownEvents.size();
	}

	public int getStartEventCount() {
		return iStartEvents.size();
	}

	public int getRemoteEventCount() {
		return iRemoteEvents.size();
	}

	public String getDefaultEvent(int aIndex) {
		if(aIndex<0||aIndex>iDefaultEvents.size()-1) return null;
		return iDefaultEvents.get(aIndex);
	}

	public String getDownEvent(int aIndex) {
		if(aIndex<0||aIndex>iDownEvents.size()-1) return null;
		return iDownEvents.get(aIndex);
	}

	public String getStartEvent(int aIndex) {
		if(aIndex<0||aIndex>iStartEvents.size()-1) return null;
		return iStartEvents.get(aIndex);
	}

	public String getRemoteEvent(int aIndex) {
		if(aIndex<0||aIndex>iRemoteEvents.size()-1) return null;
		return iRemoteEvents.get(aIndex);
	}

}
