package com.tauongames.hubble.yubl;

import android.graphics.Color;
import android.view.View;

import com.tauongames.hubble.views.YublView;

public class ViewUpdater implements Runnable {

	public enum EUpdateType {
		EBackgroundColour,
		EBackgroundImage,
		EAddView,
	}

	public static class ViewUpdaterData {

		public YublView iYublView;
		public EUpdateType iType;
		public String iProperty;
		public View iView;
	}

	ViewUpdaterData iData;

	public ViewUpdater(ViewUpdaterData aData) {
		iData=aData;
	}

	public void run() {
		switch(iData.iType) {
			case EBackgroundColour:
				iData.iYublView.setBackgroundColor(Color.parseColor(iData.iProperty));
				break;
			case EAddView:
				iData.iYublView.addView(iData.iView);
				break;
			default:
				break;
		}
	}
}
