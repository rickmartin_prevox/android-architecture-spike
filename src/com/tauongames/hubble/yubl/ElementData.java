package com.tauongames.hubble.yubl;

import java.util.ArrayList;

import android.view.View;

public class ElementData {

	public enum EElementType {
		EText,
		ESticker,
		EUnknown
	}

	private View iView;
	private ArrayList<String> iDownActions=new ArrayList<String>();

	public ElementData() {}

	public void setView(View aView) {
		iView=aView;
	}

	public View getView() {
		return iView;
	}

	public void addDownAction(final String aDownAction) {
		iDownActions.add(aDownAction);
	}

	public int getDownActionCount() {
		return iDownActions.size();
	}

	public String getDownAction(int aIndex) {
		if(aIndex<0||aIndex>iDownActions.size()-1) return null;
		return iDownActions.get(aIndex);
	}

	public String toString() {
		StringBuilder sb=new StringBuilder();
		sb.append("VIEW=").append(iView);
		for(String action: iDownActions)
			sb.append(" ACTION=").append(action);
		return sb.toString();
	}
}
