package com.tauongames.hubble.persistence;

import com.tauongames.hubble.content.WebSocketEvent;
import com.tauongames.hubble.websocket.WebSocketMediator;
import com.tauongames.hubble.websocket.WebSocketMediator.EEventType;
import com.tauongames.hubble.websocket.WebSocketMediator.WebSocketMediatorEventListener;

public class PersistenceMediator {

	static {
		registerWithWebSocketMediator();
	}

	private static WebSocketMediatorEventListenerImpl iWebSocketMedaitorEventListener=
			new WebSocketMediatorEventListenerImpl();
	private static String iLastNotificationUuid;

	private PersistenceMediator() {}

	private static class WebSocketMediatorEventListenerImpl implements WebSocketMediatorEventListener {

		@Override public void notifyWebSocketMediatorEvent(final String aUuid, WebSocketEvent aEvent) {
			if(iLastNotificationUuid!=null&&iLastNotificationUuid.equals(aUuid)) return;
			iLastNotificationUuid=aUuid;
			System.out.println("RICK: PERSISTENCE MEDIATOR GOT WEB SOCKET EVENT "+aEvent);
			switch(aEvent.getEventType()) {
				case ENewYubl:
					break;
				case ENewConversation:
					break;
				case EStickerInteraction:
					break;
				default:
					break;
			}
			// TODO: WHATEVER PERSISTING IS REQUIRED
		}
	}

	private static void registerWithWebSocketMediator() {
		WebSocketMediator.registerListener(EEventType.ENewYubl, iWebSocketMedaitorEventListener);

	}

}
