package com.tauongames.hubble.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class YublView extends FrameLayout {

	public int iWidth;
	public int iHeight;

	public YublView(Context aContext) {
		super(aContext);
	}

	public YublView(Context aContext, AttributeSet aAttrs) {
		super(aContext, aAttrs);
	}

	public YublView(Context aContext, AttributeSet aAttrs, int aDefStyle) {
		super(aContext, aAttrs, aDefStyle);
	}

}
