package com.tauongames.hubble.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundedImageView extends ImageView {

	public RoundedImageView(Context aContext) {
		super(aContext);
	}

	public RoundedImageView(Context aContext, AttributeSet aAttrs) {
		super(aContext, aAttrs);
	}

	public RoundedImageView(Context aContext, AttributeSet aAttrs, int aDefStyle) {
		super(aContext, aAttrs, aDefStyle);
	}

	@Override protected void onDraw(Canvas aCanvas) {
		Drawable drawable=getDrawable();
		if(drawable==null||getWidth()==0||getHeight()==0) return;
		Bitmap b=((BitmapDrawable) drawable).getBitmap();
		Bitmap bitmap=b.copy(Bitmap.Config.ARGB_8888, true);
		Bitmap roundBitmap=getCroppedBitmap(bitmap, getWidth());
		aCanvas.drawBitmap(roundBitmap, 0, 0, null);
	}

	public static Bitmap getCroppedBitmap(Bitmap aBitmap, int aRadius) {
		Bitmap sbmp=aBitmap;
		if(aBitmap.getWidth()!=aRadius||aBitmap.getHeight()!=aRadius)
			sbmp=Bitmap.createScaledBitmap(aBitmap, aRadius, aRadius, false);
		Bitmap output=Bitmap.createBitmap(sbmp.getWidth(), sbmp.getHeight(), Config.ARGB_8888);
		Canvas canvas=new Canvas(output);
		final Paint paint=new Paint();
		final Rect rect=new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());
		paint.setAntiAlias(true);
		paint.setFilterBitmap(true);
		paint.setDither(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(Color.parseColor("#BAB399"));
		canvas.drawCircle(sbmp.getWidth()/2+0.7f, sbmp.getHeight()/2+0.7f, sbmp.getWidth()/2+0.1f, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(sbmp, rect, rect, paint);
		return output;
	}
}