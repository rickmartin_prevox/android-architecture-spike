package com.tauongames.hubble;

import android.content.res.Resources;

import com.tauongames.hubble.content.ConversationList;
import com.tauongames.hubble.database.UserTable;

public class Globals {

	public static String iAccessToken;
	public static ConversationList iConversationList;
	public static Resources iResources;
	public static String iPackageName;
	public static int iScreenWidth;
	public static int iScreenHeight;
	public static String iUserTableName;
	public static UserTable iUserTable;
}
