package com.tauongames.hubble;

import java.util.ArrayList;

public class Constants {

	public static final String API_END_POINT="http://yublapi.herokuapp.com";
	// public static final String API_END_POINT="http://178.62.8.181:8080";
	// public static final String WEB_SOCKET_END_POINT="ws://178.62.8.181";
	public static final String WEB_SOCKET_END_POINT="ws://yublapi.herokuapp.com";
	// public static final String WEB_SOCKET_END_POINT="ws://napi.prevox.io";
	// public static final String API_END_POINT="http://api.prevox.io";

	public static final String CACHE_DIR="hubble/cache";
	public static final String EXTERNAL_ASSETS_CACHE_DIR="hubble/cache/assets";
	public static final String SHARED_PREFS_KEY="yubl";
	public static final String PREFS_ACCESS_TOKEN="access_token";
	public static final String DATABASE_NAME="yubl.db";
	public static final int DATABASE_VERSION=1;
	public static final ArrayList<String> STANDARD_ASSETS=new ArrayList<String>() {

		private static final long serialVersionUID=1L;

		{
			add("angelic1");
			add("circle");
			add("circle2");
			add("dragon");
			add("mr_parrot");
			add("rectangle");
			add("mustache_01");
			add("yubl_default1");
			add("yubl_default2");
			add("yubl_default3");
			add("yubl_default4");
			add("yubl_default5");
			add("yubl_default6");
			add("yubl_default7");
			add("yubl_default8");
			add("yubl_default9");
		}
	};
}