package com.tauongames.hubble.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import android.content.ContentValues;
import android.database.Cursor;

import com.tauongames.hubble.Globals;
import com.tauongames.hubble.database.UserTable.EUserSort;

public class UserContent {

	private HashMap<String, Object> iContentMap=new HashMap<String, Object>();

	public enum EUserContent {
		ERowId("ROW_ID", "integer primary key autoincrement", "int"),
		EUserId("USER_ID", "text", "String"),
		EUserName("USER_NAME", "text", "String");

		private String iColumnName;
		private String iSqlDataType;
		private String iJavaDataType;

		private EUserContent(final String aColumnName, final String aSqlDataType, final String aJavaDataType) {
			iColumnName=aColumnName;
			iSqlDataType=aSqlDataType;
			iJavaDataType=aJavaDataType;
		}

		public final String getColumnName() {
			return iColumnName;
		}

		public final String getSqlDataType() {
			return iSqlDataType;
		}

		public final String getJavaDataType() {
			return iJavaDataType;
		}
	}

	public void setContent(EUserContent aContent, final String aValue) {
		iContentMap.put(aContent.getColumnName(), aValue);
	}

	public void setContent(EUserContent aContent, int aValue) {
		iContentMap.put(aContent.getColumnName(), aValue);
	}

	public void setContent(EUserContent aContent, long aValue) {
		iContentMap.put(aContent.getColumnName(), aValue);
	}

	public void setContent(EUserContent aContent, boolean aValue) {
		iContentMap.put(aContent.getColumnName(), aValue?"Y":"N");
	}

	public void setContent(EUserContent aContent, Object aObj) {
		iContentMap.put(aContent.getColumnName(), null);
	}

	public void setContentNull(EUserContent aContent) {
		iContentMap.put(aContent.getColumnName(), null);
	}

	public final String getStringContent(EUserContent aContent) {
		return (String) iContentMap.get(aContent.getColumnName());
	}

	public Integer getIntContent(EUserContent aContent) {
		return((Integer) iContentMap.get(aContent.getColumnName()));
	}

	public Long getLongContent(EUserContent aContent) {
		return((Long) iContentMap.get(aContent.getColumnName()));
	}

	public Boolean getBooleanContent(EUserContent aContent) {
		String val=(String) iContentMap.get(aContent.getColumnName());
		if(val==null) return null;
		return val.equalsIgnoreCase("Y");
	}

	public void populateFromCursor(Cursor aCursor) {
		for(EUserContent content: EUserContent.values()) {
			String dataType=content.getJavaDataType();
			int index=content.ordinal();
			if(dataType.startsWith("int")) {
				setContent(content, aCursor.getInt(index));
				continue;
			}
			if(dataType.startsWith("String")) {
				setContent(content, aCursor.getString(index));
				continue;
			}
			if(dataType.startsWith("long")) {
				setContent(content, aCursor.getLong(index));
				continue;
			}
		}
	}

	public ContentValues createContentValues() {
		ContentValues values=new ContentValues();
		for(EUserContent content: EUserContent.values()) {
			if(content==EUserContent.ERowId) continue;
			String dataType=content.getJavaDataType();
			if(dataType.startsWith("int")) {
				Integer i=getIntContent(content);
				if(i==null)
					values.putNull(content.getColumnName());
				else
					values.put(content.getColumnName(), i);
				continue;
			}
			if(dataType.startsWith("String")) {
				String s=getStringContent(content);
				if(s==null)
					values.putNull(content.getColumnName());
				else
					values.put(content.getColumnName(), s);
				continue;
			}
			if(dataType.startsWith("long")) {
				Long l=getLongContent(content);
				if(l==null) values.putNull(content.getColumnName());
				values.put(content.getColumnName(), l);
				continue;
			}
		}
		return values;
	}

	@Override public String toString() {
		StringBuilder sb=new StringBuilder();
		Iterator<String> iter=iContentMap.keySet().iterator();
		while(iter.hasNext()) {
			String key=iter.next();
			Object value=iContentMap.get(key);
			sb.append(key).append("=").append(value);
			if(iter.hasNext()) sb.append(",");
		}
		return sb.toString();
	}

	public static ArrayList<UserContent> getBySortFilter(EUserSort aSort, boolean[] aFilter, boolean aUseAndFilter) {
		ArrayList<UserContent> result=new ArrayList<UserContent>();
		Cursor c=Globals.iUserTable.selectFiltered(aSort, aFilter, aUseAndFilter);
		if(c.getCount()>0&&c.moveToFirst()) {
			do {
				UserContent uc=new UserContent();
				uc.populateFromCursor(c);
				result.add(uc);
			} while(c.moveToNext());
		}
		c.close();
		return result;
	}

	public static UserContent getById(int aId) {
		return Globals.iUserTable.getById(aId);
	}

	public static void insert(ArrayList<UserContent> aContent) {
		for(UserContent uc: aContent) {
			Globals.iUserTable.insert(uc);
		}
	}
}
