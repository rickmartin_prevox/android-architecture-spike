package com.tauongames.hubble.database;

import android.database.sqlite.SQLiteDatabase;

public abstract class BaseTable {

	protected String iTableCreate;
	protected String iTableName;
	protected String[] iColumnNames;
	protected static SQLiteDatabase iDatabase;

	public void onCreate(SQLiteDatabase aDatabase) {
		// System.out.println("RICK: CREATING TABLE: "+iTableCreate);
		aDatabase.execSQL(iTableCreate);
		if(iDatabase==null) iDatabase=aDatabase;
		doFirstTime();
	}

	public void onOpen(SQLiteDatabase aDatabase) {
		iDatabase=aDatabase;
	}

	public final String getName() {
		return iTableName;
	}

	protected void doFirstTime() {}

	public static void beginTransaction() {
		iDatabase.beginTransaction();
	}

	public static void endTransaction(boolean aSuccess) {
		if(aSuccess) iDatabase.setTransactionSuccessful();
		iDatabase.endTransaction();
	}
}
