package com.tauongames.hubble.database;

import android.database.Cursor;

import com.tauongames.hubble.Globals;
import com.tauongames.hubble.database.UserContent.EUserContent;

public class UserTable extends BaseTable {

	public enum EUserFilter {
		ESentinel
	}

	public enum EUserSort {
		EAlphabetical,
		ESentinel
	}

	public UserTable() {
		Globals.iUserTableName=iTableName="user";
		Globals.iUserTable=this;
		iTableCreate="create table "+iTableName+" (";
		iColumnNames=new String[EUserContent.values().length+1];
		for(EUserContent content: EUserContent.values()) {
			iTableCreate+=content.getColumnName()+" "+content.getSqlDataType()+",";
			iColumnNames[content.ordinal()]=content.getColumnName();
		}
		iTableCreate=iTableCreate.substring(0, iTableCreate.length()-1);
		iTableCreate+=");";
	}

	protected void doFirstTime() {}

	public long insert(UserContent aContent) {
		return iDatabase.insert(iTableName, null, aContent.createContentValues());
	}

	private String buildSortString(EUserSort aSort) {
		switch(aSort) {
			case EAlphabetical:
				return EUserContent.EUserName.getColumnName();
			default:
				return null;
		}
	}

	private String buildFilterString(boolean[] aFilter, boolean aUseAnd) {
		boolean allSet=true;
		for(int a=0;a<aFilter.length;a++) {
			if(!aFilter[a]) {
				allSet=false;
				break;
			}
		}
		if(allSet&&!aUseAnd) return null;
		boolean first=true;
		StringBuilder sb=new StringBuilder();
		for(int a=0;a<aFilter.length;a++) {
			if(aFilter[a]) {
				EUserFilter filter=EUserFilter.values()[a];
				if(!first)
					sb.append(aUseAnd?" AND ":" OR ");
				else
					first=false;
				switch(filter) {
					default:
						break;
				}
			}
		}
		System.out.println("RICK: FILTER="+sb.toString());
		return sb.toString();
	}

	public Cursor selectFiltered(EUserSort aSort, boolean[] aFilter, boolean aUseAnd) {
		String sort=buildSortString(aSort);
		String filter=buildFilterString(aFilter, aUseAnd);
		return iDatabase.query(iTableName, iColumnNames, filter, null, null, null, sort);
	}

	public Cursor selectById(final int aId) {
		return iDatabase.query(iTableName, iColumnNames, EUserContent.ERowId.getColumnName()+"="+String.valueOf(aId),
				null, null, null, null);
	}

	public UserContent getById(final int aId) {
		UserContent result=null;
		Cursor c=selectById(aId);
		if(c.getCount()>0&&c.moveToNext()) {
			result=new UserContent();
			result.populateFromCursor(c);
		}
		c.close();
		return result;
	}
}
