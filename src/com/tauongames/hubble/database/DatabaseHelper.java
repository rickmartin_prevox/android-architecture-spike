package com.tauongames.hubble.database;

import java.util.ArrayList;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

	private static DatabaseHelper iInstance=null;
	private SQLiteDatabase iDatabase;
	private static String iName;
	private static int iVersion;
	private static ArrayList<BaseTable> iTables;

	public static DatabaseHelper getInstance() {
		return iInstance;
	}

	public static DatabaseHelper getInstance(Context aContext, ArrayList<BaseTable> aTables, String aName,
			int aVersion, boolean aForceReopen) {
		iTables=aTables;
		iName=aName;
		iVersion=aVersion;
		if(aForceReopen) {
			if(iInstance!=null) {
				iInstance.close();
				iInstance=null;
			}
		}
		return iInstance==null?iInstance=new DatabaseHelper(aContext, aName, null, aVersion):iInstance;
	}

	public void beginTransaction() {
		iDatabase.beginTransaction();
	}

	public void endTransaction(boolean aSuccess) {
		if(aSuccess) iDatabase.setTransactionSuccessful();
		iDatabase.endTransaction();
	}

	private DatabaseHelper(Context aContext, String aName, CursorFactory aFactory, int aVersion) {
		super(aContext, aName, null, aVersion);
		reset(aContext);
		getWritableDatabase();
	}

	@Override public void onCreate(SQLiteDatabase aDatabase) {
		iDatabase=aDatabase;
		for(BaseTable bt: iTables)
			bt.onCreate(aDatabase);
	}

	@Override public void onOpen(SQLiteDatabase aDatabase) {
		iDatabase=aDatabase;
		for(BaseTable bt: iTables)
			bt.onOpen(aDatabase);
	}

	@Override public void onUpgrade(SQLiteDatabase aDatabase, int aOldVersion, int aNewVersion) {
		for(BaseTable bt: iTables)
			aDatabase.execSQL("DROP TABLE IF EXISTS "+bt.getName());
		onCreate(aDatabase);
	}

	public BaseTable getTable(String aName) {
		for(BaseTable bt: iTables) {
			if(bt.getName().equals(aName)) return bt;
		}
		return null;
	}

	public void reset(Context aContext) {
		if(iInstance!=null) iInstance.close();
		aContext.deleteDatabase(iName);
		iInstance=null;
	}

	public int getVersion() {
		return iVersion;
	}
}
