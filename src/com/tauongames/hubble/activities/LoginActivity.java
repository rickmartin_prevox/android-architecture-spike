package com.tauongames.hubble.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.tauongames.hubble.Constants;
import com.tauongames.hubble.Globals;
import com.tauongames.hubble.R;
import com.tauongames.hubble.content.Login;
import com.tauongames.hubble.http.ApiService;
import com.tauongames.hubble.http.ApiService.ApiServiceListener;

public class LoginActivity extends Activity implements OnClickListener, ApiServiceListener {

	private Button iLoginButton;
	private String iLoginUuid;

	@Override protected void onCreate(Bundle aBundle) {
		super.onCreate(aBundle);
		setContentView(R.layout.login);
		getViews();
		setListeners();
	}

	private void getViews() {
		iLoginButton=(Button) findViewById(R.id.login_button);
	}

	private void setListeners() {
		iLoginButton.setOnClickListener(this);
	}

	@Override public void onClick(View aView) {
		if(aView==iLoginButton) {
			iLoginUuid=ApiService.login("rick2", null, "qwe123", this);
			return;
		}
	}

	// API SERVICE CALLBACKS
	@Override public void notifySuccess(String aUuid, Object aResult) {
		if(aUuid.equals(iLoginUuid)) {
			Globals.iAccessToken=((Login) aResult).getAccessToken();
			SharedPreferences prefs=getSharedPreferences(Constants.SHARED_PREFS_KEY, Context.MODE_PRIVATE);
			prefs.edit().putString(Constants.PREFS_ACCESS_TOKEN, Globals.iAccessToken).commit();
			Intent i=new Intent(LoginActivity.this, RecentsActivity.class);
			startActivity(i);
			finish();
			return;
		}
	}

	@Override public void notifyFailure(String aUuid, int aErrorCode, String aErrorMessage) {
		if(aUuid.equals(iLoginUuid)) {
			String message="FAILURE ON LOGIN "+aErrorCode+" "+aErrorMessage;
			System.out.println("RICK: "+message);
			Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
			return;
		}
	}
}
