package com.tauongames.hubble.activities;

import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;

import com.tauongames.hubble.Constants;
import com.tauongames.hubble.Globals;
import com.tauongames.hubble.R;
import com.tauongames.hubble.database.BaseTable;
import com.tauongames.hubble.database.DatabaseHelper;
import com.tauongames.hubble.database.UserTable;
import com.tauongames.hubble.utils.Utils;

public class SplashActivity extends Activity {

	private boolean iDatabaseAlreadyLoaded=false;

	@Override protected void onCreate(Bundle aBundle) {
		super.onCreate(aBundle);
		System.out.println("RICK: -------------------- APP STARTED -------------------- "+(new Date()));
		setContentView(R.layout.splash);
		Globals.iResources=getResources();
		Globals.iPackageName=getPackageName();
		DisplayMetrics metrics=new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		Globals.iScreenWidth=metrics.widthPixels;
		Globals.iScreenHeight=metrics.heightPixels;
		if(!iDatabaseAlreadyLoaded) {
			ArrayList<BaseTable> tables=new ArrayList<BaseTable>();
			// TODO: ADD NEW TABLES HERE
			tables.add(new UserTable());
			// the 'true' means to force reload the database - for debugging
			// purposes, set to false for production
			DatabaseHelper.getInstance(this, tables, Constants.DATABASE_NAME, Constants.DATABASE_VERSION, true);
			iDatabaseAlreadyLoaded=true;
		}
		LoadingThread t=new LoadingThread(this);
		t.start();
	}

	private static class LoadingThread extends Thread {

		private Activity iActivity;

		public LoadingThread(Activity aActivity) {
			iActivity=aActivity;
		}

		@Override public void run() {
			for(String baseAssetName: Constants.STANDARD_ASSETS) {
				int resId=iActivity.getResources().getIdentifier("raw/"+baseAssetName, "raw", Globals.iPackageName);
				System.out.println("RICK: BASE ASSET NAME="+baseAssetName+" "+resId);
				if(!Utils.unzip(resId, baseAssetName)) {
					System.out.println("RICK: UNABLE TO UNPACK STANDARD ASSET: "+baseAssetName);
				}
			}
			SharedPreferences prefs=iActivity.getSharedPreferences(Constants.SHARED_PREFS_KEY, Context.MODE_PRIVATE);
			Globals.iAccessToken=prefs.getString(Constants.PREFS_ACCESS_TOKEN, null);
			boolean loggedIn=Globals.iAccessToken!=null;
			Intent i=new Intent(iActivity, loggedIn?RecentsActivity.class:LoginActivity.class);
			iActivity.startActivity(i);
			iActivity.finish();

		}
	}
}
