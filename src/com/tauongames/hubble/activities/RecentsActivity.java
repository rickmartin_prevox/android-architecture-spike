package com.tauongames.hubble.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.tauongames.hubble.Globals;
import com.tauongames.hubble.R;
import com.tauongames.hubble.adapters.RecentsAdapter;
import com.tauongames.hubble.content.ConversationList;
import com.tauongames.hubble.content.WebSocketEvent;
import com.tauongames.hubble.http.ApiService;
import com.tauongames.hubble.http.ApiService.ApiServiceListener;
import com.tauongames.hubble.websocket.WebSocketManager;
import com.tauongames.hubble.websocket.WebSocketMediator;
import com.tauongames.hubble.websocket.WebSocketMediator.EEventType;
import com.tauongames.hubble.websocket.WebSocketMediator.WebSocketMediatorEventListener;

public class RecentsActivity extends Activity implements ApiServiceListener, OnItemClickListener,
		WebSocketMediatorEventListener {

	private String iConversationListUuid;
	private ListView iListView;
	private RecentsAdapter iAdapter;
	public static final String EXTRA_CONVERSATION_ID="conversation_id";
	private String iLastNotificationUuid;

	@Override protected void onCreate(Bundle aBundle) {
		super.onCreate(aBundle);
		setContentView(R.layout.recents);
		iListView=(ListView) findViewById(R.id.recents_list);
		iAdapter=new RecentsAdapter(this);
		iListView.setAdapter(iAdapter);
		iListView.setOnItemClickListener(this);
	}

	@Override public void onResume() {
		super.onResume();
		iConversationListUuid=ApiService.conversationList(this);
		// this means that we will receive new conversation events from
		// the web socket - add other events of interest here
		// see the notifyWebSocketMediatorEvent method for the handling of these
		// events
		WebSocketMediator.registerListener(EEventType.ENewConversation, this);
		WebSocketManager.connect(Globals.iAccessToken);
	}

	private void populate() {
		runOnUiThread(new Runnable() {

			@Override public void run() {
				iAdapter.setItems(Globals.iConversationList.getConversationIds());
				iAdapter.notifyDataSetChanged();
			}
		});
	}

	// API SERVICE CALLBACKS
	@Override public void notifySuccess(String aUuid, Object aResult) {
		if(aUuid.equals(iConversationListUuid)) {
			Globals.iConversationList=(ConversationList) aResult;
			populate();
			return;
		}
	}

	@Override public void notifyFailure(String aUuid, int aErrorCode, String aErrorMessage) {
		if(aUuid.equals(iConversationListUuid)) {
			System.out.println("RICK: FAILURE ON CONVERSATION LIST "+aErrorCode+" "+aErrorMessage);
			return;
		}

	}

	@Override public void onItemClick(AdapterView<?> aParent, View aView, int aPos, long aId) {
		Intent intent=new Intent(RecentsActivity.this, ConversationActivity.class);
		intent.putExtra(EXTRA_CONVERSATION_ID, (String) iAdapter.getItem(aPos));
		startActivity(intent);
	}

	@Override public void notifyWebSocketMediatorEvent(String aUuid, WebSocketEvent aEvent) {
		if(iLastNotificationUuid!=null&&iLastNotificationUuid.equals(aUuid)) return;
		iLastNotificationUuid=aUuid;
		System.out.println("RICK: ------------------------------ SOCKET EVENT IN RECENTS -------------------------- "
				+aEvent);
		switch(aEvent.getEventType()) {
			case ENewConversation:
				iConversationListUuid=ApiService.conversationList(this);
				break;
			default:
				break;
		}
	}
}
