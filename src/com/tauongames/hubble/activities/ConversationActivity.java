package com.tauongames.hubble.activities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.UUID;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.tauongames.hubble.Globals;
import com.tauongames.hubble.R;
import com.tauongames.hubble.content.ConversationList;
import com.tauongames.hubble.content.ConversationSummary;
import com.tauongames.hubble.content.WebSocketEvent;
import com.tauongames.hubble.content.YublSummary;
import com.tauongames.hubble.http.ApiService;
import com.tauongames.hubble.http.ApiService.ApiServiceListener;
import com.tauongames.hubble.utils.ActionManager;
import com.tauongames.hubble.utils.ZipManager;
import com.tauongames.hubble.utils.ZipManager.ZipManagerData;
import com.tauongames.hubble.utils.ZipManager.ZipManagerListener;
import com.tauongames.hubble.views.YublView;
import com.tauongames.hubble.websocket.WebSocketMediator;
import com.tauongames.hubble.websocket.WebSocketMediator.EEventType;
import com.tauongames.hubble.websocket.WebSocketMediator.WebSocketMediatorEventListener;
import com.tauongames.hubble.yubl.ElementData;
import com.tauongames.hubble.yubl.YublBuilder;
import com.tauongames.hubble.yubl.YublData;

/**
 * 
 * This is the main activity for displaying yubls When populating this view it
 * first requests a view containing a placeholder This request is sent to
 * YublBuilder - the view returned is reused as required Once the placeholder is
 * in place, the zip file is asynchronously processed We use a UUID to match the
 * request to the response. And we keep a map object for this purpose. When the
 * zip file has been processed, a request a made to YublBuilder to create the
 * real yubl from the zip file. This updates the placeholder view to contain the
 * elements that make up a real yubl. It also creates a YublData object which
 * contains the necessary information to interact with the yubl.
 * 
 * @author Rick
 * 
 */
public class ConversationActivity extends Activity implements ZipManagerListener, OnClickListener, ApiServiceListener,
		WebSocketMediatorEventListener {

	private String iConversationId;
	private ConversationSummary iConvSummary;
	private LinearLayout iConversationContentLayout;
	private ArrayList<YublView> iYubls=new ArrayList<YublView>();
	// this is only used to map the zip file async call to the view
	private HashMap<String, UuidViewMapData> iUuidMap=new HashMap<String, UuidViewMapData>();
	private String iInteractionUuid;
	private String iLastNotificationUuid;
	// the set of known yubls in this conversation
	private HashMap<String, YublData> iYublData=new HashMap<String, YublData>();
	private String iConversationListUuid;
	private ScrollView iScrollView;

	private static class UuidViewMapData {

		public String iId;
		public YublView iView;
	}

	@Override protected void onCreate(Bundle aBundle) {
		super.onCreate(aBundle);
		setContentView(R.layout.conversation);
		iConversationId=getIntent().getStringExtra(RecentsActivity.EXTRA_CONVERSATION_ID);
		iConvSummary=Globals.iConversationList.getConversation(iConversationId);
		iConversationContentLayout=(LinearLayout) findViewById(R.id.conversation_content_layout);
		iScrollView=(ScrollView) findViewById(R.id.conversation_scroll_view);
	}

	@Override public void onResume() {
		super.onResume();
		// this means that we will receive sticker interaction and
		// new yubl events from the web socket - add other events of interest
		// here see the notifyWebSocketMediatorEvent method for the handling of
		// these events
		WebSocketMediator.registerListener(EEventType.EStickerInteraction, this);
		WebSocketMediator.registerListener(EEventType.ENewYubl, this);
		populate();
	}

	private void populate() {
		// remove all the known yubls from the set
		iYublData.clear();
		iConversationContentLayout.removeAllViews();
		Set<String> ids=iConvSummary.getYublIds();
		for(String id: ids) {
			YublSummary ys=iConvSummary.getYubl(id);
			YublView yubl=YublBuilder.getYublPlaceholder(this, ys);
			iConversationContentLayout.addView(yubl);
			LinearLayout ll=new LinearLayout(this);
			LayoutParams lp=new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 5);
			ll.setLayoutParams(lp);
			ll.setBackgroundColor(Color.BLACK);
			iConversationContentLayout.addView(ll);
			iYubls.add(yubl);
			String uuid=UUID.randomUUID().toString();
			// uuid is replaced if we have to go to the server
			// see below
			ZipManagerData zmd=new ZipManagerData();
			zmd.iCacheSubDir=id;
			zmd.iListener=this;
			zmd.iUuid=uuid;
			UuidViewMapData uvmd=new UuidViewMapData();
			uvmd.iId=id;
			uvmd.iView=yubl;
			iUuidMap.put(uuid, uvmd); // getYublZipFile might send notification
										// synchronously and we need to have
										// saved the uuid
			System.out.println("RICK: ABOUT TO UNZIP "+id);
			uuid=ZipManager.getYublZipFile(zmd, iConversationId, id);
			iUuidMap.put(uuid, uvmd); // if a different uuid was returned
										// by getYublZipFile we need to update
										// the map
		}
	}

	/**
	 * Called when the zip has been loaded successfully This asks the
	 * YublBuilder to replace the existing view with real yubl elements. A
	 * YublData object is created which is the subset of yubl information needed
	 * by this activity to handle element interactions etc.
	 */
	@Override public void notifyZipLoadingSuccess(String aUuid) {
		System.out.println("RICK: ZIP LOADED OK");
		UuidViewMapData uvmd=iUuidMap.get(aUuid);
		if(uvmd==null) return;
		YublData yublData=YublBuilder.getRealYubl(this, uvmd.iId, uvmd.iView, this);
		if(yublData==null) return;
		// add the yubl data to the set of known yubls
		iYublData.put(uvmd.iId, yublData);
		// no longer need the uuid map entry
		iUuidMap.remove(aUuid);
	}

	/**
	 * The zipfile load failed.. TODO: handle this properly
	 */
	@Override public void notifyZipLoadingFailure(String aUuid, int aErrorCode, String aErrorMessage) {
		UuidViewMapData uvmd=iUuidMap.get(aUuid);
		if(uvmd==null) return;
		System.out.println("RICK: FAILED TO GET ZIP FILE FOR "+aUuid+" "+aErrorCode+" "+aErrorMessage);
		// no longer need the uuid map entry
		iUuidMap.remove(aUuid);
	}

	/**
	 * When the user clicks on something then we need to determine what was
	 * clicked and then look up what to do about it
	 */
	@Override public void onClick(View aView) {
		String yublId=getYublId(aView);
		if(yublId==null) {
			System.out.println("RICK: UNABLE TO FIND YUBL WITH VIEW: "+aView);
			return;
		}
		int index=getElementIndex(yublId, aView);
		ElementData ed=getElementData(yublId, index);
		System.out.println("RICK: ELEMENT DATA="+yublId+" "+index+" "+ed);
		int downCount=ed.getDownActionCount();
		if(downCount==0) return;
		for(int a=0;a<downCount;a++) {
			ActionManager.doAction(this, ed.getDownAction(a), aView);
			// TODO: SEND APPROPRIATE MESSAGE TO SERVER NOT JUST STICKER
			// INTERACTION
			System.out.println("RICK: NOTIFYING SERVER OF YUBL INTERACTION: "+yublId+" "+index);
			iInteractionUuid=ApiService.stickerInteraction(iConversationId, yublId, String.valueOf(index), this);
		}
	}

	private ElementData getElementData(final String aYublId, int aIndex) {
		YublData yd=iYublData.get(aYublId);
		return yd.getElementData(aIndex);
	}

	private String getYublId(View aView) {
		Iterator<YublData> iter=iYublData.values().iterator();
		while(iter.hasNext()) {
			YublData yd=iter.next();
			if(yd.getElementData(aView)!=null) return yd.getId();
		}
		return null;
	}

	private int getElementIndex(final String aYublId, View aView) {
		YublData yd=iYublData.get(aYublId);
		return yd.getElementIndex(aView);
	}

	// Restful api callbacks
	@Override public void notifySuccess(String aUuid, Object aResult) {
		if(aUuid.equals(iInteractionUuid)) {
			System.out.println("RICK: SUCCESS SENDING INTERACTION");
			return;
		}
		if(aUuid.equals(iConversationListUuid)) {
			// TODO: this is brute force... need to just add the new YUBL to the
			// end of the list - see populate()
			Globals.iConversationList=(ConversationList) aResult;
			iConvSummary=Globals.iConversationList.getConversation(iConversationId);
			populate();
			return;
		}
	}

	@Override public void notifyFailure(String aUuid, int aErrorCode, String aErrorMessage) {
		if(aUuid.equals(iInteractionUuid)) {
			System.out.println("RICK: FAILURE SENDING INTERACTION "+aErrorCode+" "+aErrorMessage);
			return;
		}
	}

	/**
	 * Called when a web socket event is triggered The uuid is retained to avoid
	 * multiple handling of the same event (not really a likely occurrence but
	 * better to catch it). Also steps are taken in the mediator to catch
	 * duplicates
	 */
	@Override public void notifyWebSocketMediatorEvent(final String aUuid, WebSocketEvent aEvent) {
		if(iLastNotificationUuid!=null&&iLastNotificationUuid.equals(aUuid)) return;
		iLastNotificationUuid=aUuid;
		// not interested in events for other conversations
		if(!iConversationId.equals(aEvent.getConversationId())) return;
		System.out
				.println("RICK: ------------------------------ SOCKET EVENT IN CONVERSATION -------------------------- "
						+aEvent);
		switch(aEvent.getEventType()) {
			case EStickerInteraction:
				YublData yd=iYublData.get(aEvent.getYublId());
				if(yd==null) return;
				ElementData ed=getElementData(aEvent.getYublId(), aEvent.getStickerId());
				System.out.println("RICK: ELEMENT DATA="+aEvent.getYublId()+" "+aEvent.getStickerId()+" "+ed);
				int downCount=ed.getDownActionCount();
				System.out.println("RICK: DOWN COUNT="+downCount);
				if(downCount==0) return;
				for(int a=0;a<downCount;a++)
					ActionManager.doAction(this, ed.getDownAction(a), ed.getView());
				return;
			case ENewYubl:
				String convId=aEvent.getConversationId();
				// only interested in yubls for this conversation
				if(convId==null||!convId.equals(iConversationId)) return;
				// TODO: this is brute force... need to just update the existing
				// conversation list with the new yubl
				iConversationListUuid=ApiService.conversationList(this);
				return;
			case EVote:
				// TODO: ONCE I HAVE THE API SPEC
				return;
			default:
				System.out.println("RICK: CONVERSATION NOT HANDLING WS EVENTS OF TYPE "+aEvent.getEventType());
				return;
		}
	}
}
