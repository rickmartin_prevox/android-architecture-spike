package com.tauongames.hubble.utils;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtils {

	public static String getString(JSONObject aJObj, final String aName, final String aDefault) throws JSONException {
		if(aJObj==null||aName==null) return aDefault;
		if(aJObj.has(aName)) return aJObj.getString(aName);
		return aDefault;
	}

	public static boolean getBoolean(JSONObject aJObj, final String aName, final boolean aDefault) throws JSONException {
		if(aJObj==null||aName==null) return aDefault;
		if(aJObj.has(aName)) return aJObj.getBoolean(aName);
		return aDefault;
	}

	public static int getInteger(JSONObject aJObj, final String aName, final int aDefault) throws JSONException {
		if(aJObj==null||aName==null) return aDefault;
		if(aJObj.has(aName)) return aJObj.getInt(aName);
		return aDefault;
	}

	public static float getFloat(JSONObject aJObj, final String aName, final float aDefault) throws JSONException {
		if(aJObj==null||aName==null) return aDefault;
		if(aJObj.has(aName)) return (float) aJObj.getDouble(aName);
		return aDefault;
	}
}
