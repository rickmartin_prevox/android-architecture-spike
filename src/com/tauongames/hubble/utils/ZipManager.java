package com.tauongames.hubble.utils;

import java.util.HashMap;

import com.tauongames.hubble.Constants;
import com.tauongames.hubble.http.ApiService;
import com.tauongames.hubble.http.ApiService.ApiServiceListener;

public class ZipManager {

	public interface ZipManagerListener {

		public void notifyZipLoadingSuccess(final String aUuid);

		public void notifyZipLoadingFailure(final String aUuid, int aErrorCode, final String aErrorMessage);
	}

	public static class ZipManagerData {

		public String iUuid;
		public String iCacheSubDir;
		public ZipManagerListener iListener;
	}

	private static HashMap<String, ZipManagerData> iDataMap=new HashMap<String, ZipManagerData>();
	private static ApiServiceListenerImpl iApiServiceListener=new ApiServiceListenerImpl();

	private ZipManager() {}

	public static String getYublZipFile(final ZipManagerData aData, final String aConversationId, final String aYublId) {
		boolean fileExists=false;
		if(fileExists) {
			aData.iListener.notifyZipLoadingSuccess(aData.iUuid);
			return aData.iUuid;
		}
		String uuid=ApiService.yublZip(aConversationId, aYublId, iApiServiceListener);
		iDataMap.put(uuid, aData);
		return uuid;
	}

	private static boolean unzip(ZipManagerData aData, byte[] aResult) {
		return Utils.unzip(aResult, Constants.CACHE_DIR+"/"+aData.iCacheSubDir);
	}

	private static class ApiServiceListenerImpl implements ApiServiceListener {

		@Override public void notifySuccess(String aUuid, Object aResult) {
			ZipManagerData data=iDataMap.get(aUuid);
			if(data!=null) {
				boolean rc=unzip(data, (byte[]) aResult);
				if(rc)
					data.iListener.notifyZipLoadingSuccess(aUuid);
				else
					data.iListener.notifyZipLoadingFailure(aUuid, -1, "unable to unzip");
				iDataMap.remove(aUuid);
			}
		}

		@Override public void notifyFailure(String aUuid, int aErrorCode, String aErrorMessage) {
			ZipManagerData data=iDataMap.get(aUuid);
			if(data!=null) data.iListener.notifyZipLoadingFailure(aUuid, aErrorCode, aErrorMessage);
			iDataMap.remove(aUuid);
		}
	}
}