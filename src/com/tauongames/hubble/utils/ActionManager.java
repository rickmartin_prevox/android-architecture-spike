package com.tauongames.hubble.utils;

import android.app.Activity;
import android.view.View;

import com.tauongames.hubble.animation.AnimationManager;
import com.tauongames.hubble.animation.AnimationManager.EAnimationType;

public class ActionManager {

	private enum EAction {
		EAnimate("anim"),
		EUnknown("unknown");

		private String iName;

		private EAction(final String aName) {
			iName=aName;
		}

		public static EAction getByName(final String aName) {
			for(EAction a: EAction.values())
				if(a.iName.equals(aName)) return a;
			return EUnknown;
		}
	}

	private ActionManager() {}

	public static void doAction(Activity aContext, final String aAction, final View aView) {
		if(aAction==null||aAction.length()==0||aView==null) return;
		String[] tokens=aAction.split(":");
		EAction action=EAction.getByName(tokens[0]);
		switch(action) {
			case EAnimate:
				final EAnimationType animType=EAnimationType.getByName(tokens[1]);
				aContext.runOnUiThread(new Runnable() {

					@Override public void run() {
						if(animType!=EAnimationType.EUnknown) AnimationManager.animate(animType, aView);
					}
				});
				break;
			default:
				break;
		}
	}
}
