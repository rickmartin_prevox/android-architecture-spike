package com.tauongames.hubble.utils;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.graphics.Bitmap;

public class MemoryCache {

	private Map<String, Bitmap> iCache=Collections.synchronizedMap(new LinkedHashMap<String, Bitmap>(10, 1.5f, true));
	private long iSize=0;
	private long iLimit=1000000;

	public MemoryCache() {
		setLimit(Runtime.getRuntime().maxMemory()/4);
	}

	public void setLimit(long aNewLimit) {
		iLimit=aNewLimit;
	}

	public Bitmap get(String aId) {
		try {
			if(!iCache.containsKey(aId)) return null;
			return iCache.get(aId);
		} catch(NullPointerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void put(String aId, Bitmap aBitmap) {
		try {
			if(iCache.containsKey(aId)) iSize-=getSizeInBytes(iCache.get(aId));
			iCache.put(aId, aBitmap);
			iSize+=getSizeInBytes(aBitmap);
			checkSize();
		} catch(Throwable th) {
			th.printStackTrace();
		}
	}

	private void checkSize() {
		if(iSize>iLimit) {
			Iterator<Entry<String, Bitmap>> iter=iCache.entrySet().iterator();
			while(iter.hasNext()) {
				Entry<String, Bitmap> entry=iter.next();
				iSize-=getSizeInBytes(entry.getValue());
				iter.remove();
				if(iSize<=iLimit) break;
			}
		}
	}

	public void clear() {
		try {
			iCache.clear();
			iSize=0;
		} catch(NullPointerException ex) {
			ex.printStackTrace();
		}
	}

	long getSizeInBytes(Bitmap aBitmap) {
		if(aBitmap==null) return 0;
		return aBitmap.getRowBytes()*aBitmap.getHeight();
	}
}