package com.tauongames.hubble.utils;

import java.io.File;

import android.content.Context;

import com.tauongames.hubble.Constants;

public class FileCache {

	private File iCacheDir;

	public FileCache(Context aContext) {
		if(android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
			iCacheDir=new File(android.os.Environment.getExternalStorageDirectory(), Constants.CACHE_DIR);
		else
			iCacheDir=aContext.getCacheDir();
		if(!iCacheDir.exists()) iCacheDir.mkdirs();
	}

	public File getFile(String aUrl) {
		String filename=String.valueOf(aUrl.hashCode());
		File f=new File(iCacheDir, filename);
		return f;
	}

	public void clear() {
		File[] files=iCacheDir.listFiles();
		if(files==null) return;
		for(File f: files)
			f.delete();
	}
}