package com.tauongames.hubble.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import com.tauongames.hubble.Constants;
import com.tauongames.hubble.Globals;

public class Utils {

	public static void copyStream(InputStream iIs, OutputStream iOs) {
		final int buffer_size=1024;
		try {
			byte[] bytes=new byte[buffer_size];
			for(;;) {
				int count=iIs.read(bytes, 0, buffer_size);
				if(count==-1) break;
				iOs.write(bytes, 0, count);
			}
		} catch(Exception ex) {}
	}

	public static byte[] toByteArray(InputStream aInputStream) throws IOException {
		ByteArrayOutputStream buffer=new ByteArrayOutputStream();
		int nRead;
		byte[] data=new byte[16384];
		while((nRead=aInputStream.read(data, 0, data.length))!=-1) {
			buffer.write(data, 0, nRead);
		}
		buffer.flush();
		return buffer.toByteArray();
	}

	public static boolean unzip(byte[] aData, String aExtractToFolder) {
		File cacheDir=new File(android.os.Environment.getExternalStorageDirectory(), aExtractToFolder);
		if(cacheDir.exists()) return true;
		cacheDir.mkdirs();
		InputStream is=new ByteArrayInputStream(aData);
		ZipInputStream zin=new ZipInputStream(is);
		ZipEntry ze=null;
		if(is!=null&&zin!=null) {
			try {
				while((ze=zin.getNextEntry())!=null) {
					FileOutputStream fout=new FileOutputStream(cacheDir.getAbsolutePath()+"/"+ze.getName());
					for(int c=zin.read();c!=-1;c=zin.read())
						fout.write(c);
					zin.closeEntry();
					fout.close();
				}
			} catch(IOException e) {
				e.printStackTrace();
				System.out.println("RICK: ERROR: "+e.toString());
				return false;
			}
			try {
				zin.close();
			} catch(IOException e) {
				e.printStackTrace();
				return false;
			}
			return true;
		}
		return false;
	}

	public static boolean unzip(int aResId, String aExtractToFolder) {
		String path=
				android.os.Environment.getExternalStorageDirectory()+"/"+Constants.EXTERNAL_ASSETS_CACHE_DIR+"/"
						+aExtractToFolder;
		File cacheDir=new File(path);
		if(cacheDir.exists()) return true;
		File macDir=new File(path+"/__MACOSX");
		macDir.mkdirs();
		ZipInputStream zin=new ZipInputStream(Globals.iResources.openRawResource(aResId));
		ZipEntry ze=null;
		try {
			while((ze=zin.getNextEntry())!=null) {
				System.out.println("RICK: GOT FILE: "+ze.getName());
				try {
					FileOutputStream fout=new FileOutputStream(cacheDir.getAbsolutePath()+"/"+ze.getName());
					for(int c=zin.read();c!=-1;c=zin.read())
						fout.write(c);
					zin.closeEntry();
					fout.close();
				} catch(Exception ex) {
					continue;
				}
			}
		} catch(Exception ex) {}
		try {
			zin.close();
		} catch(IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
