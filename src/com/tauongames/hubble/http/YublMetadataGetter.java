package com.tauongames.hubble.http;

import java.io.UnsupportedEncodingException;

public class YublMetadataGetter extends HttpGetter {

	public YublMetadataGetter(final String aUuid, final String aAccessToken, final String aConversationId,
			String aYublId, HttpGetterListener aListener) throws UnsupportedEncodingException {
		super(aListener, aUuid, "conversations/"+aConversationId+"/yubls/"+"552bd5004ce8520300e142bf/data",
				aAccessToken);
	}
}
