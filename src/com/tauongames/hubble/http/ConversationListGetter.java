package com.tauongames.hubble.http;

import java.io.UnsupportedEncodingException;

public class ConversationListGetter extends HttpGetter {

	public ConversationListGetter(final String aUuid, final String aAccessToken, HttpGetterListener aListener)
			throws UnsupportedEncodingException {
		super(aListener, aUuid, "conversations", aAccessToken);
	}
}
