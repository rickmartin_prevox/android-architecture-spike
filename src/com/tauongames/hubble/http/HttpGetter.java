package com.tauongames.hubble.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.tauongames.hubble.Constants;
import com.tauongames.hubble.utils.Utils;

public abstract class HttpGetter extends Thread {

	public interface HttpGetterListener {

		public void notifySuccess(final String aUuid, final String aResult);

		public void notifySuccess(final String aUuid, final byte[] aResult);

		public void notifyFailure(final String aUuid, int aErrorCode, final String aErrorMessage);
	}

	private HttpGet iHttpGet;
	private HttpClient iHttpClient;
	protected String iUuid;
	private HttpGetterListener iListener;
	private boolean iBinary=false;

	protected HttpGetter(final HttpGetterListener aListener, final String aUuid, final String aEndPoint,
			final String aAccessToken) {
		iListener=aListener;
		iUuid=aUuid;
		iHttpClient=new DefaultHttpClient();
		iHttpGet=new HttpGet();
		String uri=Constants.API_END_POINT+"/"+aEndPoint;
		// System.out.println("RICK: GETTER URI="+uri+" "+aAccessToken);
		try {
			iHttpGet.setURI(new URI(uri));
			iHttpGet.setHeader("Content-Type", "application/json");
			iHttpGet.setHeader("Accept", "application/json");
			if(aAccessToken!=null) iHttpGet.setHeader("access_token", aAccessToken);
		} catch(Exception ex) {
			System.out.println("RICK: HTTP POSTER: ERROR SETTING URI: "+ex.toString());
			notifyFailure(-1, ex.toString());
		}
	}

	@Override public void run() {
		BufferedReader br=null;
		try {
			HttpResponse resp=iHttpClient.execute(iHttpGet);
			if(resp==null) {
				notifyFailure(-1, "RESPONSE NULL");
				return;
			}
			int rc=resp.getStatusLine().getStatusCode();
			if(rc!=200) {
				String rcStr=resp.getStatusLine().getReasonPhrase();
				notifyFailure(rc, rcStr);
				return;
			}
			if(!iBinary) {
				br=new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer sb=new StringBuffer();
				String line=null;
				while((line=br.readLine())!=null)
					sb.append(line);
				notifySuccess(sb.toString());
			} else {
				try {
					byte[] bytes=Utils.toByteArray(resp.getEntity().getContent());
					// System.out.println("RICK: GOT BINARY DATA "+bytes.length);
					notifySuccess(bytes);
				} catch(IOException ex) {
					notifyFailure(-1, ex.toString());
				}
			}
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println("RICK: HTTPGETTER: ERROR GETTING RESPONSE: "+ex.toString());
			notifyFailure(-1, ex.toString());
		} finally {
			if(br!=null) {
				try {
					br.close();
				} catch(Exception ex) {
					notifyFailure(-1, ex.toString());
				}
			}
		}
	}

	public void setBinary(boolean aFlag) {
		iBinary=aFlag;
	}

	private void notifyFailure(int aErrorCode, final String aErrorMessage) {
		if(iListener!=null) iListener.notifyFailure(iUuid, aErrorCode, aErrorMessage);
	}

	private void notifySuccess(final String aResult) {
		if(iListener!=null) iListener.notifySuccess(iUuid, aResult);
	}

	private void notifySuccess(final byte[] aResult) {
		if(iListener!=null) iListener.notifySuccess(iUuid, aResult);
	}

}
