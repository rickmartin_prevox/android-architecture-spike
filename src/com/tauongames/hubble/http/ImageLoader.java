package com.tauongames.hubble.http;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;

import com.tauongames.hubble.R;
import com.tauongames.hubble.utils.FileCache;
import com.tauongames.hubble.utils.MemoryCache;
import com.tauongames.hubble.utils.Utils;

public class ImageLoader {

	private MemoryCache iMemoryCache=new MemoryCache();
	private FileCache iFileCache;
	private Map<ImageView, String> iImageViews=Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
	private ExecutorService iExecutorService;
	private Handler iHandler=new Handler();
	private static final int STUB_ID=R.drawable.profile_placeholder;

	public ImageLoader(Context aContext) {
		iFileCache=new FileCache(aContext);
		iExecutorService=Executors.newFixedThreadPool(5);
	}

	public void displayImage(String aUrl, ImageView aImageView) {
		iImageViews.put(aImageView, aUrl);
		Bitmap bitmap=iMemoryCache.get(aUrl);
		if(bitmap!=null)
			aImageView.setImageBitmap(bitmap);
		else {
			queuePhoto(aUrl, aImageView);
			aImageView.setImageResource(STUB_ID);
		}
	}

	private void queuePhoto(String aUrl, ImageView aImageView) {
		PhotoToLoad p=new PhotoToLoad(aUrl, aImageView);
		iExecutorService.submit(new PhotosLoader(p));
	}

	private Bitmap getBitmap(String aUrl) {
		File f=iFileCache.getFile(aUrl);
		Bitmap b=decodeFile(f);
		if(b!=null) return b;
		try {
			Bitmap bitmap=null;
			URL imageUrl=new URL(aUrl);
			HttpURLConnection conn=(HttpURLConnection) imageUrl.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is=conn.getInputStream();
			OutputStream os=new FileOutputStream(f);
			Utils.copyStream(is, os);
			os.close();
			conn.disconnect();
			bitmap=decodeFile(f);
			return bitmap;
		} catch(Throwable ex) {
			ex.printStackTrace();
			if(ex instanceof OutOfMemoryError) iMemoryCache.clear();
			return null;
		}
	}

	private Bitmap decodeFile(File aFile) {
		try {
			BitmapFactory.Options o=new BitmapFactory.Options();
			o.inJustDecodeBounds=true;
			FileInputStream stream1=new FileInputStream(aFile);
			BitmapFactory.decodeStream(stream1, null, o);
			stream1.close();
			final int REQUIRED_SIZE=70;
			int width_tmp=o.outWidth, height_tmp=o.outHeight;
			int scale=1;
			while(true) {
				if(width_tmp/2<REQUIRED_SIZE||height_tmp/2<REQUIRED_SIZE) break;
				width_tmp/=2;
				height_tmp/=2;
				scale*=2;
			}
			BitmapFactory.Options o2=new BitmapFactory.Options();
			o2.inSampleSize=scale;
			FileInputStream stream2=new FileInputStream(aFile);
			Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
			stream2.close();
			return bitmap;
		} catch(FileNotFoundException e) {} catch(IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private class PhotoToLoad {

		public String iUrl;
		public ImageView iImageView;

		public PhotoToLoad(String aUrl, ImageView aImageView) {
			iUrl=aUrl;
			iImageView=aImageView;
		}
	}

	class PhotosLoader implements Runnable {

		private PhotoToLoad iPhotoToLoad;

		PhotosLoader(PhotoToLoad aPhotoToLoad) {
			iPhotoToLoad=aPhotoToLoad;
		}

		@Override public void run() {
			try {
				if(imageViewReused(iPhotoToLoad)) return;
				Bitmap bmp=getBitmap(iPhotoToLoad.iUrl);
				iMemoryCache.put(iPhotoToLoad.iUrl, bmp);
				if(imageViewReused(iPhotoToLoad)) return;
				BitmapDisplayer bd=new BitmapDisplayer(bmp, iPhotoToLoad);
				iHandler.post(bd);
			} catch(Throwable th) {
				th.printStackTrace();
			}
		}
	}

	boolean imageViewReused(PhotoToLoad aPhotoToLoad) {
		String tag=iImageViews.get(aPhotoToLoad.iImageView);
		if(tag==null||!tag.equals(aPhotoToLoad.iUrl)) return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {

		private Bitmap iBitmap;
		private PhotoToLoad iPhotoToLoad;

		public BitmapDisplayer(Bitmap aBitmap, PhotoToLoad aPhoto) {
			iBitmap=aBitmap;
			iPhotoToLoad=aPhoto;
		}

		public void run() {
			if(imageViewReused(iPhotoToLoad)) return;
			if(iBitmap!=null)
				iPhotoToLoad.iImageView.setImageBitmap(iBitmap);
			else
				iPhotoToLoad.iImageView.setImageResource(STUB_ID);
		}
	}

	public void clearCache() {
		iMemoryCache.clear();
		iFileCache.clear();
	}

}
