package com.tauongames.hubble.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import com.tauongames.hubble.Constants;

public abstract class HttpPoster extends Thread {

	public interface HttpPosterListener {

		public void notifySuccess(final String aUuid, final String aResult);

		public void notifyFailure(final String aUuid, int aErrorCode, final String aErrorMessage);
	}

	private HttpPost iHttpPost;
	private HttpClient iHttpClient;
	protected String iUuid;
	private HttpPosterListener iListener;

	protected HttpPoster(final HttpPosterListener aListener, final String aUuid, final String aEndPoint,
			final String aAccessToken) {
		iListener=aListener;
		iUuid=aUuid;
		iHttpClient=new DefaultHttpClient();
		iHttpPost=new HttpPost();
		String uri=Constants.API_END_POINT+"/"+aEndPoint;
		System.out.println("RICK: POSTER URI="+uri);
		try {
			iHttpPost.setURI(new URI(uri));
			iHttpPost.setHeader("Content-Type", "application/json");
			iHttpPost.setHeader("Accept", "application/json");
			if(aAccessToken!=null) iHttpPost.setHeader("access_token", aAccessToken);
		} catch(Exception ex) {
			System.out.println("RICK: HTTP POSTER: ERROR SETTING URI: "+ex.toString());
			notifyFailure(-1, ex.toString());
		}
	}

	public void setBody(final JSONObject aBody) throws UnsupportedEncodingException {
		if(aBody==null||aBody.length()==0) return;
		iHttpPost.setEntity(new ByteArrayEntity(aBody.toString().getBytes("UTF8")));
	}

	public void setBody(final String aBody) throws UnsupportedEncodingException {
		if(aBody==null||aBody.length()==0) return;
		iHttpPost.setEntity(new ByteArrayEntity(aBody.getBytes("UTF8")));
	}

	@Override public void run() {
		BufferedReader br=null;
		try {
			HttpResponse resp=iHttpClient.execute(iHttpPost);
			if(resp==null) {
				notifyFailure(-1, "RESPONSE NULL");
				return;
			}
			int rc=resp.getStatusLine().getStatusCode();
			if(rc!=200) {
				String rcStr=resp.getStatusLine().getReasonPhrase();
				notifyFailure(rc, rcStr);
				return;
			}
			br=new BufferedReader(new InputStreamReader(resp.getEntity().getContent()));
			StringBuffer sb=new StringBuffer();
			String line=null;
			while((line=br.readLine())!=null)
				sb.append(line);
			notifySuccess(sb.toString());
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println("RICK: HTTPPOSTER: ERROR GETTING RESPONSE: "+ex.toString());
		} finally {
			if(br!=null) {
				try {
					br.close();
				} catch(Exception ex) {}
			}
		}
	}

	private void notifyFailure(int aErrorCode, final String aErrorMessage) {
		if(iListener!=null) iListener.notifyFailure(iUuid, aErrorCode, aErrorMessage);
	}

	private void notifySuccess(final String aResult) {
		if(iListener!=null) iListener.notifySuccess(iUuid, aResult);
	}
}
