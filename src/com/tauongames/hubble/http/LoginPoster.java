package com.tauongames.hubble.http;

import java.io.UnsupportedEncodingException;

public class LoginPoster extends HttpPoster {

	public LoginPoster(final String aUuid, final String aUserName, final String aPhone, final String aPassword,
			HttpPosterListener aListener) throws UnsupportedEncodingException {
		super(aListener, aUuid, "authorize", null);
		setBody("{\"username\": \""+aUserName+"\",\"phone_number\": \"\",\"password\": \""+aPassword+"\"}");
	}
}
