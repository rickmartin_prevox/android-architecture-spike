package com.tauongames.hubble.http;

import java.io.UnsupportedEncodingException;

public class YublZipGetter extends HttpGetter {

	public YublZipGetter(final String aUuid, final String aAccessToken, final String aConversationId,
			final String iYublId, HttpGetterListener aListener) throws UnsupportedEncodingException {
		super(aListener, aUuid, "conversations/"+aConversationId+"/yubls/"+iYublId+"/data", aAccessToken);
	}
}
