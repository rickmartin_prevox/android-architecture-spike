package com.tauongames.hubble.http;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;

import com.tauongames.hubble.utils.FileCache;
import com.tauongames.hubble.utils.Utils;

public class ZipLoader {

	public interface ZipLoaderListener {

		public void notifyZipLoadingSucceeded(final String aUuid);

		public void notifyZipLoadingFailed(final String aUuid, int aErrorCode, final String aErrorMessage);
	}

	private FileCache iFileCache;
	private ExecutorService iExecutorService;
	private String iUuid;
	private ZipLoaderListener iListener;

	public ZipLoader(Context aContext, final String aUuid, ZipLoaderListener aListener) {
		iUuid=aUuid;
		iListener=aListener;
		iFileCache=new FileCache(aContext);
		iExecutorService=Executors.newFixedThreadPool(5);
	}

	public void getZip(String aUrl) {
		File f=iFileCache.getFile(aUrl);
		if(f!=null) return;
		queueDownload(aUrl);
	}

	private void queueDownload(String aUrl) {
		iExecutorService.submit(new ZipLoaderTask(aUrl));
	}

	private void getZipFile(String aUrl) {
		File f=iFileCache.getFile(aUrl);
		if(f!=null) return;
		try {
			System.out.println("RICK: LOADING ZIP FROM "+aUrl);
			URL zipUrl=new URL(aUrl);
			HttpURLConnection conn=(HttpURLConnection) zipUrl.openConnection();
			conn.setConnectTimeout(30000);
			conn.setReadTimeout(30000);
			conn.setInstanceFollowRedirects(true);
			InputStream is=conn.getInputStream();
			OutputStream os=new FileOutputStream(f);
			Utils.copyStream(is, os);
			os.close();
			conn.disconnect();
			if(iListener!=null) iListener.notifyZipLoadingSucceeded(iUuid);
		} catch(Throwable ex) {
			ex.printStackTrace();
			if(iListener!=null) iListener.notifyZipLoadingFailed(iUuid, -1, ex.toString());
		}
	}

	class ZipLoaderTask implements Runnable {

		private String iZipToLoad;

		ZipLoaderTask(String aZipToLoad) {
			iZipToLoad=aZipToLoad;
		}

		@Override public void run() {
			try {
				getZipFile(iZipToLoad);
			} catch(Throwable th) {
				th.printStackTrace();
			}
		}
	}
}
