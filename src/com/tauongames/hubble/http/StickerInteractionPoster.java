package com.tauongames.hubble.http;

import java.io.UnsupportedEncodingException;

public class StickerInteractionPoster extends HttpPoster {

	public StickerInteractionPoster(final String aUuid, final String aConversationId, final String aYublId,
			final String aStickerId, final String aAccessToken, HttpPosterListener aListener)
			throws UnsupportedEncodingException {
		super(aListener, aUuid, "conversations/"+aConversationId+"/yubls/"+aYublId+"/sticker/"+aStickerId, aAccessToken);
	}
}
