package com.tauongames.hubble.http;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.UUID;

import org.json.JSONException;

import com.tauongames.hubble.Globals;
import com.tauongames.hubble.content.ConversationList;
import com.tauongames.hubble.content.Login;
import com.tauongames.hubble.http.ApiService.HttpGetterListenerImpl.EGetApi;
import com.tauongames.hubble.http.ApiService.HttpPosterListenerImpl.EPostApi;
import com.tauongames.hubble.http.HttpGetter.HttpGetterListener;
import com.tauongames.hubble.http.HttpPoster.HttpPosterListener;

public class ApiService {

	public interface ApiServiceListener {

		public void notifySuccess(String aUuid, Object aResult);

		public void notifyFailure(String aUuid, int aErrorCode, String aErrorMessage);
	}

	private static HttpPosterListenerImpl iPosterListener=new HttpPosterListenerImpl();
	private static HttpGetterListenerImpl iGetterListener=new HttpGetterListenerImpl();
	private static HashMap<String, ApiServiceListener> iListeners=new HashMap<String, ApiServiceListener>();

	private ApiService() {}

	public static class HttpPosterListenerImpl implements HttpPosterListener {

		public enum EPostApi {
			ELogin,
			EStickerInteraction
		}

		private HashMap<String, EPostApi> iUuidMap=new HashMap<String, EPostApi>();

		@Override public void notifySuccess(String aUuid, String aResult) {
			if(iUuidMap.containsKey(aUuid)) {
				System.out.println("RICK: POST API SERVICE SUCCESS "+iUuidMap.get(aUuid));
				switch(iUuidMap.get(aUuid)) {
					case ELogin:
						if(iListeners.containsKey(aUuid)) {
							try {
								iListeners.get(aUuid).notifySuccess(aUuid, new Login(aResult));
							} catch(JSONException ex) {
								iListeners.get(aUuid).notifyFailure(aUuid, -1, ex.toString());
							}
							iListeners.remove(aUuid);
						}
						break;
					case EStickerInteraction:
						if(iListeners.containsKey(aUuid)) {
							iListeners.get(aUuid).notifySuccess(aUuid, aResult);
							iListeners.remove(aUuid);
						}
						break;

					default:
						break;
				}
				iUuidMap.remove(aUuid);
			}
		}

		@Override public void notifyFailure(String aUuid, int aErrorCode, String aErrorMessage) {
			System.out.println("RICK: POST API SERVICE FAILURE "+aErrorMessage);
			if(iUuidMap.containsKey(aUuid)) {
				iListeners.get(aUuid).notifyFailure(aUuid, aErrorCode, aErrorMessage);
				iListeners.remove(aUuid);
				iUuidMap.remove(aUuid);
			}
		}

		public void addListener(EPostApi aApi, final String aUuid, final ApiServiceListener aListener) {
			iUuidMap.put(aUuid, aApi);
			iListeners.put(aUuid, aListener);
		}

	}

	public static class HttpGetterListenerImpl implements HttpGetterListener {

		public enum EGetApi {
			EConversationList,
			EConversation,
			EYublMetadata,
			EYublZip
		}

		private HashMap<String, EGetApi> iUuidMap=new HashMap<String, EGetApi>();

		@Override public void notifySuccess(String aUuid, String aResult) {
			if(iUuidMap.containsKey(aUuid)) {
				System.out.println("RICK: GET API SERVICE SUCCESS "+iUuidMap.get(aUuid));
				switch(iUuidMap.get(aUuid)) {
					case EConversationList:
						if(iListeners.containsKey(aUuid)) {
							try {
								iListeners.get(aUuid).notifySuccess(aUuid, new ConversationList(aResult));
							} catch(JSONException ex) {
								iListeners.get(aUuid).notifyFailure(aUuid, -1, ex.toString());
							}
							iListeners.remove(aUuid);
						}
						break;
					case EConversation:
						if(iListeners.containsKey(aUuid)) {
							iListeners.get(aUuid).notifySuccess(aUuid, aResult);
						}
						break;
					case EYublMetadata:
						if(iListeners.containsKey(aUuid)) {
							iListeners.get(aUuid).notifySuccess(aUuid, aResult);
						}
						break;
					default:
						break;
				}
				iUuidMap.remove(aUuid);
			}
		}

		@Override public void notifyFailure(String aUuid, int aErrorCode, String aErrorMessage) {
			System.out.println("RICK: GET API SERVICE FAILURE "+aErrorMessage);
			if(iUuidMap.containsKey(aUuid)) {
				iListeners.get(aUuid).notifyFailure(aUuid, aErrorCode, aErrorMessage);
				iListeners.remove(aUuid);
				iUuidMap.remove(aUuid);
			}
		}

		public void addListener(EGetApi aApi, final String aUuid, final ApiServiceListener aListener) {
			iUuidMap.put(aUuid, aApi);
			iListeners.put(aUuid, aListener);
		}

		@Override public void notifySuccess(String aUuid, byte[] aResult) {
			if(iUuidMap.containsKey(aUuid)) {
				System.out.println("RICK: GET API SERVICE SUCCESS (BINARY) "+iUuidMap.get(aUuid));
				switch(iUuidMap.get(aUuid)) {
					case EYublZip:
						if(iListeners.containsKey(aUuid)) {
							iListeners.get(aUuid).notifySuccess(aUuid, aResult);
						}
						break;
					default:
						break;
				}
				iUuidMap.remove(aUuid);
			}
		}

	}

	public static String login(String aUserName, String aPhone, String aPassword, ApiServiceListener aListener) {
		try {
			String uuid=UUID.randomUUID().toString();
			iPosterListener.addListener(EPostApi.ELogin, uuid, aListener);
			LoginPoster lp=new LoginPoster(uuid, aUserName, aPhone, aPassword, iPosterListener);
			lp.start();
			return uuid;
		} catch(UnsupportedEncodingException ex) {
			return null;
		}
	}

	public static String conversationList(ApiServiceListener aListener) {
		try {
			String uuid=UUID.randomUUID().toString();
			iGetterListener.addListener(EGetApi.EConversationList, uuid, aListener);
			ConversationListGetter clg=new ConversationListGetter(uuid, Globals.iAccessToken, iGetterListener);
			clg.start();
			return uuid;
		} catch(UnsupportedEncodingException ex) {
			return null;
		}
	}

	public static String conversation(String aConversationId, ApiServiceListener aListener) {
		try {
			String uuid=UUID.randomUUID().toString();
			iGetterListener.addListener(EGetApi.EConversation, uuid, aListener);
			ConversationGetter cg=new ConversationGetter(uuid, Globals.iAccessToken, aConversationId, iGetterListener);
			cg.start();
			return uuid;
		} catch(UnsupportedEncodingException ex) {
			return null;
		}
	}

	public static String yublMetadata(String aConversationId, String aYublId, ApiServiceListener aListener) {
		try {
			String uuid=UUID.randomUUID().toString();
			iGetterListener.addListener(EGetApi.EYublMetadata, uuid, aListener);
			YublMetadataGetter ymg=
					new YublMetadataGetter(uuid, Globals.iAccessToken, aConversationId, aYublId, iGetterListener);
			ymg.start();
			return uuid;
		} catch(UnsupportedEncodingException ex) {
			return null;
		}
	}

	public static String yublZip(String aConversationId, String aYublId, ApiServiceListener aListener) {
		try {
			String uuid=UUID.randomUUID().toString();
			iGetterListener.addListener(EGetApi.EYublZip, uuid, aListener);
			YublZipGetter yzg=new YublZipGetter(uuid, Globals.iAccessToken, aConversationId, aYublId, iGetterListener);
			yzg.setBinary(true);
			yzg.start();
			return uuid;
		} catch(UnsupportedEncodingException ex) {
			return null;
		}
	}

	public static String stickerInteraction(String aConversationId, String aYublId, String aStickerId,
			ApiServiceListener aListener) {
		try {
			String uuid=UUID.randomUUID().toString();
			iPosterListener.addListener(EPostApi.EStickerInteraction, uuid, aListener);
			StickerInteractionPoster sip=
					new StickerInteractionPoster(uuid, aConversationId, aYublId, aStickerId, Globals.iAccessToken,
							iPosterListener);
			sip.start();
			return uuid;
		} catch(UnsupportedEncodingException ex) {
			return null;
		}
	}
}
