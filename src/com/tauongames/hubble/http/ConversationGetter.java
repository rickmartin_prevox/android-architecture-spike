package com.tauongames.hubble.http;

import java.io.UnsupportedEncodingException;

public class ConversationGetter extends HttpGetter {

	public ConversationGetter(final String aUuid, final String aAccessToken, final String aConversationId,
			HttpGetterListener aListener) throws UnsupportedEncodingException {
		super(aListener, aUuid, "conversations/"+aConversationId, aAccessToken);
	}
}
