package com.tauongames.hubble.websocket;

import java.net.URI;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;

import com.tauongames.hubble.Constants;
import com.tauongames.hubble.content.WebSocketEvent;

/**
 * This needs to live for the lifetime of the application
 * 
 */
public class WebSocketManager {

	private static boolean iConnected=false;

	private static class WebSocketClientListener implements WebSocketClient.Listener {

		private String iAccessToken;

		public WebSocketClientListener(final String aAccessToken) {
			iAccessToken=aAccessToken;
		}

		public void setAccessToken(final String aAccessToken) {
			iAccessToken=aAccessToken;
		}

		public void onConnect() {
			System.out.println("RICK: GOT ON-CONNECT");
			iConnected=true;
		}

		public void onMessage(String aMessage) {
			if(aMessage==null||aMessage.length()==0) return;
			try {
				WebSocketMediator.notifyWebSocketEvent(new WebSocketEvent(aMessage));
			} catch(JSONException ex) {
				ex.printStackTrace();
				return;
			}
		}

		// binary data not expected
		public void onMessage(byte[] aData) {}

		public void onDisconnect(int aCode, String aReason) {
			iConnected=false;
			connect(iAccessToken);
		}

		public void onError(Exception aError) {
			System.out.println("RICK: WEB SOCKET ON ERROR "+aError.toString());
		}
	}

	private static WebSocketClientListener mListener;
	private static WebSocketClient mClient;

	/**
	 * Connect to the end point
	 * 
	 * @return true if connected ok else false on error
	 */
	public static boolean connect(String aAccessToken) {
		if(iConnected) return true;
		System.out.println("RICK: ABOUT TO CONNECT "+(new Date()));
		try {
			URI uri=new URI(Constants.WEB_SOCKET_END_POINT);
			List<BasicNameValuePair> extraHeaders=new ArrayList<BasicNameValuePair>();
			if(mListener==null) mListener=new WebSocketClientListener(aAccessToken);
			if(mClient==null) mClient=new WebSocketClient(uri, mListener, extraHeaders, aAccessToken);
			mClient.connect(); // calls back on onError() if fails.
		} catch(Exception ex) {
			ex.printStackTrace();
			return false;
		}
		return true;
	}

	public static void setAccessToken(final String aAccessToken) {
		if(mListener!=null) mListener.setAccessToken(aAccessToken);
	}
}
