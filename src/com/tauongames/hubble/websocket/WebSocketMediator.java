package com.tauongames.hubble.websocket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import com.tauongames.hubble.content.WebSocketEvent;

public class WebSocketMediator {

	public enum EEventType {
		ENewYubl("new_yubl"),
		ENewConversation("new_conversation"),
		EStickerInteraction("sticker_interaction"),
		EVote("vote"),
		EUnknown("unknown");

		private String iName;

		private EEventType(final String aName) {
			iName=aName;
		}

		public static EEventType getByName(final String aName) {
			for(EEventType et: EEventType.values())
				if(et.iName.equals(aName)) return et;
			return EUnknown;
		}
	}

	private static HashMap<EEventType, ArrayList<WebSocketMediatorEventListener>> iListeners=
			new HashMap<EEventType, ArrayList<WebSocketMediatorEventListener>>();
	// we sometimes get the same event more than once - ditch duplicates
	private static final long EVENT_COOL_OFF_TIME=1000;
	private static WebSocketEvent iLastEventReceived;
	private static long iLastEventReceivedTimestamp;

	public interface WebSocketMediatorEventListener {

		public void notifyWebSocketMediatorEvent(final String aUuid, WebSocketEvent aEvent);
	}

	private WebSocketMediator() {}

	private static void notifyEvent(final String aUuid, WebSocketEvent aEvent) {
		if(!iListeners.containsKey(aEvent.getEventType())) return;
		long timeSinceLastEvent=System.currentTimeMillis()-iLastEventReceivedTimestamp;
		// we sometimes get duplicate events - just allow then to pass
		if(timeSinceLastEvent<EVENT_COOL_OFF_TIME) {
			if(iLastEventReceived!=null&&iLastEventReceived.equals(aEvent)) return;
		}
		iLastEventReceived=aEvent;
		iLastEventReceivedTimestamp=System.currentTimeMillis();
		for(WebSocketMediatorEventListener wsmel: iListeners.get(aEvent.getEventType())) {
			System.out.println("RICK: NOTIFYING EVENT*****");
			wsmel.notifyWebSocketMediatorEvent(aUuid, aEvent);
		}
	}

	public static void notifyWebSocketEvent(final WebSocketEvent aEvent) {
		String uuid=UUID.randomUUID().toString();
		if(aEvent.getEventType()==EEventType.EUnknown) return;
		notifyEvent(uuid, aEvent);
	}

	public static void registerListener(EEventType aType, WebSocketMediatorEventListener aListener) {
		if(aType==EEventType.EUnknown||aListener==null) return;
		System.out.println("RICK: REGISTERING WSM EVENT LISTENER: "+aType+" "+aListener);
		ArrayList<WebSocketMediatorEventListener> al=iListeners.get(aType);
		if(al==null) {
			al=new ArrayList<WebSocketMediatorEventListener>();
			iListeners.put(aType, al);
		}
		if(!al.contains(aListener)) al.add(aListener);
	}

	public static void unregisterListener(EEventType aType, WebSocketMediatorEventListener aListener) {
		if(aType==EEventType.EUnknown||aListener==null||!iListeners.containsKey(aType)) return;
		if(iListeners.get(aType).contains(aListener)) iListeners.get(aType).remove(aListener);
	}
}
